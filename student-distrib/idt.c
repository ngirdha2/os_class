#include "x86_desc.h"
#include "idt.h"
#include "lib.h"
#include "interrupt_mod/interrupt_wrappers.h"
#include "syscall_mod/syscall_wrappers.h"
#include "types.h"
//#include "keyboard_module/keyboard_driver.h"

extern uint32_t system_halt(pcb_t* process);

/* Segment selector values */
#define KERNEL_CS   0x0010      //16
#define KERNEL_DS   0x0018      //24
#define USER_CS     0x0023      //35 0010 0011
#define USER_DS     0x002B      //43 0010 1011
#define KERNEL_TSS  0x0030      //48
#define KERNEL_LDT  0x0038      //56

// Declare static variables for this file
static void DEFAULT();
static void DIVIDE_ERROR_EXCEPTION();
static void DEBUG_EXCEPTION();
static void NMI_INTERRUPT();
static void BREAKPOINT_EXCEPTION();
static void OVERFLOW_EXCEPTION();
static void BOUND_RANGE_EXCEEDED_EXCEPTION();
static void INVALID_OPCODE_EXCEPTION();
static void DEVICE_NOT_AVAILABLE_EXCEPTION();
static void DOUBLE_FAULT_EXCEPTION();
static void COPROCESSOR_SEGMENT_OVERUN();
static void INVALID_TSS_EXCEPTION();
static void SEGMENT_NOT_PRESENT();
static void STACK_FAULT_EXCEPTION();
static void GENERAL_PROTECTION_EXCEPTION();
static void PAGE_FAULT_EXCEPTION();
static void ASSERTION_FAILURE();
static void X87_FPU_FLOATING_POINT_ERROR();
static void ALIGNMENT_CHECK_EXCEPTION();
static void MACHINE_CHECK_EXCEPTION();
static void SIMD_FLOATING_POINT_EXCEPTION();
static void EXCEPTION_HALT();

/* init_idt
 *
 * Set appropriate values for the idt and load the idt
 * Inputs: None
 * Outputs: None
 * Side Effects: Initializes the IDT for the OS
 *
 */
void init_idt(){
    int i;
    // Initialize all cells in array to default values
    for(i = 0; i < NUM_VEC; i++){

      idt[i].seg_selector = KERNEL_CS;
      idt[i].reserved4 = 0;
      idt[i].reserved3 = 0; //fix later
      idt[i].reserved2 = 1;
      idt[i].reserved1 = 1;
      idt[i].size = 1;
      idt[i].reserved0 = 0;
      idt[i].dpl = 0;
      idt[i].present = 1;
      //else idt[i].present = 0;
      SET_IDT_ENTRY(idt[i], DEFAULT);

    }
    // Change priority level in system call cell
    idt[128].dpl = 3;


    // Link each cell that is being used to the correct functor
    SET_IDT_ENTRY(idt[0], DIVIDE_ERROR_EXCEPTION);
    SET_IDT_ENTRY(idt[1], DEBUG_EXCEPTION);
    SET_IDT_ENTRY(idt[2], NMI_INTERRUPT);
    SET_IDT_ENTRY(idt[3], BREAKPOINT_EXCEPTION);
    SET_IDT_ENTRY(idt[4], OVERFLOW_EXCEPTION);
    SET_IDT_ENTRY(idt[5], BOUND_RANGE_EXCEEDED_EXCEPTION);
    SET_IDT_ENTRY(idt[6], INVALID_OPCODE_EXCEPTION);
    SET_IDT_ENTRY(idt[7], DEVICE_NOT_AVAILABLE_EXCEPTION);
    SET_IDT_ENTRY(idt[8], DOUBLE_FAULT_EXCEPTION);
    SET_IDT_ENTRY(idt[9], COPROCESSOR_SEGMENT_OVERUN);
    SET_IDT_ENTRY(idt[10], INVALID_TSS_EXCEPTION);
    SET_IDT_ENTRY(idt[11], SEGMENT_NOT_PRESENT);
    SET_IDT_ENTRY(idt[12], STACK_FAULT_EXCEPTION);
    SET_IDT_ENTRY(idt[13], GENERAL_PROTECTION_EXCEPTION);
    SET_IDT_ENTRY(idt[14], PAGE_FAULT_EXCEPTION);
    SET_IDT_ENTRY(idt[15], ASSERTION_FAILURE);
    SET_IDT_ENTRY(idt[16], X87_FPU_FLOATING_POINT_ERROR);
    SET_IDT_ENTRY(idt[17], ALIGNMENT_CHECK_EXCEPTION);
    SET_IDT_ENTRY(idt[18], MACHINE_CHECK_EXCEPTION);
    SET_IDT_ENTRY(idt[19], SIMD_FLOATING_POINT_EXCEPTION);

    // Interrupts
    SET_IDT_ENTRY(idt[32], TIMER_CHIP_INTERRUPT_WRAPPER);
    SET_IDT_ENTRY(idt[33], KEYBOARD_INTERRUPT_WRAPPER);
    SET_IDT_ENTRY(idt[40], REAL_TIME_CLOCK_INTERRUPT_WRAPPER);
    SET_IDT_ENTRY(idt[128],SYSTEM_CALL_WRAPPER);

    // Now load the idt at the given pointer
    lidt(idt_desc_ptr);
}

/* DEFAULT_INTERRUPT
 *
 * Default functor for any idt entry
 * that is not intialized
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints functor name to screen
 *
 */
static void DEFAULT(){
  printf("DEFAULT INTERRUPT\n");
  EXCEPTION_HALT();

}

/* DIVIDE_ERROR_EXCEPTION
 *
 * Functor for idt entry 0
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void DIVIDE_ERROR_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");



  printf("DIVIDE ERROR EXCEPTION\n");
  EXCEPTION_HALT();


}
/* DEBUG_EXCEPTION
 *
 * Functor for idt entry 1
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void DEBUG_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("DEBUG EXCEPTION\n");
  EXCEPTION_HALT();

}
/* NMI_INTERRUPT
 *
 * Functor for idt entry 2
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void NMI_INTERRUPT(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("NMI INTERRUPT\n");
  EXCEPTION_HALT();

}
/* BREAKPOINT_EXCEPTION
 *
 * Functor for idt entry 3
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void BREAKPOINT_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("BREAKPOINT EXCEPTION\n");
  EXCEPTION_HALT();

}
/* OVERFLOW_EXCEPTION
 *
 * Functor for idt entry 4
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void OVERFLOW_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("OVERFLOW EXCEPTION\n");
  EXCEPTION_HALT();

}
/* BOUND_RANGE_EXCEEDED_EXCEPTION
 *
 * Functor for idt entry 5
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void BOUND_RANGE_EXCEEDED_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("BOUND RANGE EXCEEDED EXCEPTION\n");
  EXCEPTION_HALT();

}
/* INVALID_OPCODE_EXCEPTION
 *
 * Functor for idt entry 6
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void INVALID_OPCODE_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("INVALID OPCODE EXCEPTION\n");
  EXCEPTION_HALT();

}
/* DEVICE_NOT_AVAILABLE_EXCEPTION
 *
 * Functor for idt entry 7
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void DEVICE_NOT_AVAILABLE_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("DEVICE NOT AVAILABLE EXCEPTION\n");
  EXCEPTION_HALT();

}
/* DOUBLE_FAULT_EXCEPTION
 *
 * Functor for idt entry 8
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void DOUBLE_FAULT_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("DOUBLE FAULT EXCEPTION\n");
  EXCEPTION_HALT();

}
/* COPROCESSOR_SEGMENT_OVERUN
 *
 * Functor for idt entry 9
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void COPROCESSOR_SEGMENT_OVERUN(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("COPROCESSOR SEGMENT OVERUN\n");
  EXCEPTION_HALT();

}
/* INVALID_TSS_EXCEPTION
 *
 * Functor for idt entry 10
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void INVALID_TSS_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");

  //
//

  printf("INVALID TSS EXCEPTION\n");
  EXCEPTION_HALT();

}
/* SEGMENT_NOT_PRESENT
 *
 * Functor for idt entry 11
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void SEGMENT_NOT_PRESENT(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("SEGMENT NOT PRESENT\n");
  EXCEPTION_HALT();

}
/* STACK_FAULT_EXCEPTION
 *
 * Functor for idt entry 12
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void STACK_FAULT_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("STACK FAULT EXCEPTION\n");
  EXCEPTION_HALT();

}
/* GENERAL_PROTECTION_EXCEPTION
 *
 * Functor for idt entry 13
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void GENERAL_PROTECTION_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");

  //
  //

  printf("GENERAL PROTECTION EXCEPTION\n");
  EXCEPTION_HALT();

}
/* PAGE_FAULT_EXCEPTION
 *
 * Functor for idt entry 14
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void PAGE_FAULT_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");

  //
  //


  uint32_t cr0_val, cr2_val, cr3_val, cr4_val;

	asm volatile (	"					\n\
					MOV %%cr0, %%eax	\n\
					MOVL %%eax, %0		\n\
					MOV %%cr2, %%eax	\n\
					MOVL %%eax, %1		\n\
					MOV %%cr3, %%eax	\n\
					MOVL %%eax, %2		\n\
					MOV %%cr4, %%eax	\n\
					MOVL %%eax, %3		\n\
					"
					: "=r"(cr0_val), "=r"(cr2_val) , "=r"(cr3_val), "=r"(cr4_val)
					: /* no input */
					: "%eax"
					);
  printf("PAGE FAULT EXCEPTION ");
	printf("CR0: 0x%x, CR2: 0x%x, CR3: 0x%x, CR4: 0x%x\n", cr0_val, cr2_val, cr3_val, cr4_val );

  for(;;);
  EXCEPTION_HALT();
  //EXCEPTION_HALT();

}
/* ASSERTION_FAILURE
 *
 * Functor for idt entry 15
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void ASSERTION_FAILURE(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("ASSERTION FAILURE\n");
  EXCEPTION_HALT();

}
/* X87_FPU_FLOATING_POINT_ERROR
 *
 * Functor for idt entry 16
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void X87_FPU_FLOATING_POINT_ERROR(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("X87 FPU FLOATING POINT ERROR\n");
  EXCEPTION_HALT();

}
/* ALIGNMENT_CHECK_EXCEPTION
 *
 * Functor for idt entry 17
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void ALIGNMENT_CHECK_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("ALIGNMENT CHECK EXCEPTION\n");
  EXCEPTION_HALT();

}
/* MACHINE_CHECK_EXCEPTION
 *
 * Functor for idt entry 18
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void MACHINE_CHECK_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("MACHINE CHECK EXCEPTION\n");
  EXCEPTION_HALT();

}
/* SIMD_FLOATING_POINT_EXCEPTION
 *
 * Functor for idt entry 19
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints Functor name
 *
 */
static void SIMD_FLOATING_POINT_EXCEPTION(){
  //asm volatile("movw %w0, %%ds": : "r" (KERNEL_DS) : "memory");




  printf("SIMD FLOATING POINT EXCEPTION\n");
  EXCEPTION_HALT();

}

/* init_rtc
 *
 * Initializes the rtc for the PIC
 * Inputs: None
 * Outputs: None
 * Side Effects: Intialization of RTC
 * BASED OFF OF OS DEV EXAMPLE CODE
 *
 *
void init_rtc(){
  outb(0x8B, 0x70);		// select register B, and disable NMI
  char prev = inb(0x71);	// read the current value of register B
  outb(0x8B, 0x70);		// set the index again (a read will reset the index to register D)
  outb(prev | 0x40, 0x71);	// write the previous value ORed with 0x40. This turns on bit 6 of register B


  uint8_t rate = 14;
  rate &= 0x0F;			// rate must be above 2 and not over 15
  outb(0x8A, 0x70);		// set index to register A, disable NMI
  prev=inb(0x71);	// get initial value of register A
  outb(0x8A, 0x70);		// reset index to A
  outb((prev & 0xF0) | rate, 0x71); //write only our rate to A. Note, rate is the bottom 4 bits.
}*/


static void EXCEPTION_HALT(){
  pcb_t* process;
  if((process = get_curr_pcb()) == 0) return; //Get PCB of current process which is to be halted

  uint32_t ret = system_halt(process);
  asm volatile(
  "movl %0, %%esp; \
  popal;\
  movl $256, %%eax; \
  leave; \
  ret;  \
  "
       :"=r"(ret)
  );
}
