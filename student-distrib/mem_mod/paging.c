/* Paging.c - This file implements all of the paging function*/

#include "../multiboot.h"
#include "../x86_desc.h"
#include "../lib.h"
#include "paging.h"

#define POWER_2_10 	1024
#define Global		8
#define PageSize 	7
#define Accessed 	6	//Only in Page Directory
#define Dirty		6	//Only in Page Table
#define CacheNot 	4
#define WriteTh 	3 //This is Write through enable bit
#define User 		2
#define Writeable 	1

//CR4 register
#define PGE			7 //Page Global Enable bit
#define PSE			4 //To enable both 4KB and 4MB pages
#define PAE			5 //Set CR4.PAE to 0 for 32-bit paging
//#define LME		 //Dont need CR4.LME = 0 for now for 32bit paging

//CR0 register
#define NE			5 //Numeric error is bit 5 in CR0
#define PG			31//Paging enabled bit is 31
#define PE			0 //Protection enable bit is 0
#define WP			16//Write protect bit is 16

#define VMEM_BASE		0xB8000	  //Video Memory Base address
#define VMEM_PT_PD_IDX	33 //(0x8400000 / 0x400000) 33 or 132MiB
#define VMEM_PAGE_ADDR 	0x84B8000
#define VMEM_PAGE_SIZE 		 0x1000
//#define PDIR_BASE		0x77FF000  //Page Directory Base address
//#define PTAB_BASE		0x7FE000  //Page Table Base address
#define KERNEL_BASE		0x400000  //Kernel Base
#define PROCESS_BASE	0x800000  //8MB - 1st Process Base
#define PROGRAM_BASE 0x8000000 // 128MB

#define PT_Mask			  0x3FF000 	//Mask to remove bottom 12 bits and high bits in address
#define PD_12_Mask		0xFFFFF000  //Mask to remove bottom 12 bits
#define PD_22_Mask		0xFFC00000  //Mask to remove bottom 22 bits

#define idx			184
#define align		4096

/*
//Pseudo code
	#Check CR3 contains physical address of first paging structure
	#CR4.PSE = 1 to enable both 4KB pages and 4MB pages
	#CR4.PGE = 1 for global kernel page
	#A lot more user and supervisor protection mode bits
	#Check CR0.PE should be 1 before paging can be enabled
	#CR0.WP is for Write protection
	#Then use MOV to CR0.PG to 1 to enable paging


	# 4 KB Page Directory has physical address thats in 31:12 of CR3
	# Page Directory has 1024 32-bit entries of PDE
	# It will have an global page entry for kernel page at physical address 0x400000 (4MB), and this page will be size 4MB
	# 1st User level program (shell) should be loaded at physical address 0x800000 (8MB)
	# 2nd User level program should be loaded at physical 0xC00000 (12MB)

Page Directory
1024 entries * 32 bit each (10bit for address, and 22 bits for supervisor, protection etc etc
so 4kB size. Saving it as part of kernel from 0x7FF000 to 0x800000. So make CR3 = 0x7FF000

Page Table
1024 entries * 32 bit each, so 4kB size.
Saving it as part of kernel from 0x7FE000 to 0x7FF000
*/

static uint32_t page_directory[POWER_2_10] __attribute__((aligned(align))) ;
static uint32_t page_table[POWER_2_10]     __attribute__((aligned(align))) ;

/* This function initializes pages. It sets 4kB page for video memory, and 4MB page for kernel
	It sets a new page directory with 2 entries and 1 page table with 1 entry.
	input - none
	ouput - none
	return value - none
	side effects - It modifies the CR0, CR3 and CR4 registers
*/
void init_paging(void){
	//static uint32_t page_directory[1024] __attribute__( (aligned (4)));
	//static uint32_t page_table[1024];
	uint32_t cr0_val, cr2_val, cr3_val, cr4_val;

	asm volatile (	"					\n\
					MOV %%cr0, %%eax	\n\
					MOVL %%eax, %0		\n\
					MOV %%cr2, %%eax	\n\
					MOVL %%eax, %1		\n\
					MOV %%cr3, %%eax	\n\
					MOVL %%eax, %2		\n\
					MOV %%cr4, %%eax	\n\
					MOVL %%eax, %3		\n\
					"
					: "=r"(cr0_val), "=r"(cr2_val) , "=r"(cr3_val), "=r"(cr4_val)
					: /* no input */
					: "%eax"
					);
	printf("Reading CR0=%d CR2=%d CR3=%d CR4=%d\n", cr0_val, cr2_val, cr3_val, cr4_val );


	//static uint32_t* page_directory = (uint32_t*) 0x77FF000;

	//Each index in page directory refers to 4MB of address space
	memset(page_directory, 0, sizeof(uint32_t) * POWER_2_10); //Set all bits in pagetable to 0 to be sure
	//page_directory[0] = (PTAB_BASE & PD_12_Mask)   | (0<<PageSize) | (0<<CacheNot) | (0<<WriteTh) | (1<<User) | (1<<Writeable) | 1; // Entry for 4KB pages
	page_directory[0] = ( ((uint32_t)page_table) & PD_12_Mask)   | (0<<PageSize) | (0<<CacheNot) | (0<<WriteTh) | (1<<User) | (1<<Writeable) | 1; // Entry for 4KB pages
	page_directory[1] = (KERNEL_BASE & PD_22_Mask) | (1<<PageSize) | (0<<CacheNot) | (0<<WriteTh) | (0<<User) | (1<<Writeable) | 1; //Entry for 4MB Global Kernel page
	page_directory[33] = ( ((uint32_t)page_table) & PD_12_Mask)   | (0<<PageSize) | (0<<CacheNot) | (0<<WriteTh) | (1<<User) | (1<<Writeable) | 1; // Entry for 4KB pages

	//This is page table for 4kB pages from 0-4MB
	//static uint32_t* page_table = (uint32_t*) PTAB_BASE;
	//0x400000 to 0x8000000 is kernel page and should be present
	//page_table[0]   = 0; //0MB to B8000 should not be present
	//page_table[183] = 0; //Before B8000 should not be present
	memset(page_table, 0, sizeof(uint32_t) * POWER_2_10); //Set all bits in pagetable to 0 to be sure
	// B8000 to B9000 is video memory and should be present
	page_table[idx] = (VMEM_BASE & PT_Mask) | (1<<Global) | (0<<Dirty) | (0<<CacheNot) | (0<<WriteTh) | (1<<User) | (1<<Writeable) | 1;
	//vid_page_table[idx] = (VMEM_BASE & PT_Mask) | (1<<Global) | (0<<Dirty) | (0<<CacheNot) | (0<<WriteTh) | (1<<User) | (1<<Writeable) | 1;
	//page_table[185] = 0; // B9000 to 0x400000 should not be present

	printf("Starting paging. Setting CR3, CR4 and CR0\n");
	cr3_val = (((uint32_t)page_directory) & PD_12_Mask) | (1<<4); //disable caching of page directory
	cr4_val = (1 << PGE) | (1 << PSE) | (0 << PAE);
	cr0_val = (1 << PG ) | (1 << WP ) | (1 << NE) | (1<<PE); 	//Setting PG and PE bit. Also set bit 16 Write Protect
	asm volatile (	"					\n\
					MOV %0, %%cr3		\n\
					MOV %1, %%cr4		\n\
					MOV %2, %%cr0		\n\
					"
					:  /* no output*/
					: "r"(cr3_val), "r"(cr4_val) , "r"(cr0_val)
					);
	asm volatile (	"					\n\
					MOV %%cr0, %0		\n\
					MOV %%cr2, %1		\n\
					MOV %%cr3, %2		\n\
					MOV %%cr4, %3		\n\
					"
					: "=r"(cr0_val), "=r"(cr2_val) , "=r"(cr3_val), "=r"(cr4_val)
					: /* no input */
					);
	printf("Paging done Reading CR0=%d CR2=%d CR3=%d CR4=%d\n", cr0_val, cr2_val, cr3_val, cr4_val );
}

/* This function will add 4MB page with an entry in page directory and page table for new process.
	Also when ending process, restore parent's page using this function
	input - process_num -
	ouput - none
	return value - 0 if successful, 1 if not
	side effects - Makes change in page directory.
*/
void change_process_page(const uint32_t process_num) {
	//bool big - to decide between 4kB and 4MB page size
	//allocate 4MB page
	uint32_t physical_addr = PROCESS_BASE + (process_num * 0x400000);
	uint32_t virtual_addr = PROGRAM_BASE;// + (process_num * 0x400000);
	uint32_t index = virtual_addr / 0x400000; //32;   128MB / 4MB;
	//Entry for 4MB page for process
	page_directory[index] = (physical_addr & PD_22_Mask) | (1<<PageSize) | (0<<CacheNot) | (0<<WriteTh) | (1<<User) | (1<<Writeable) | 1;
	uint32_t cr3_val = (((uint32_t)page_directory) & PD_12_Mask) | (1<<4); //disable caching of page directory
	//Setting CR3 register again to force TLB Flush and set page directory again
	asm volatile (	"				\n\
					MOV %0, %%cr3	\n\
					"
					: /* no output */
					: "r"(cr3_val)
					);
	//printf("Added 4MB page for process %d\n", process_num);
}


/* This process changes terminal page
 * input - terminal number
 * output - none
 * return value - none
 * side effects - Changed terminal page
 */
void change_terminal_page(uint32_t terminal){
		//pcb_t* process_ = process_queue_[curr_process]; //switch to this later

  //this will fin
  //while(terminal_process->parent_pcb != NULL) terminal_process = terminal_process->parent_pcb;

	uint32_t process_vid_base = VMEM_BASE + (terminal *  VMEM_PAGE_SIZE);

	page_table[idx] = (process_vid_base & PT_Mask) | (1<<Global) | (0<<Dirty) | (0<<CacheNot) | (0<<WriteTh) | (1<<User) | (1<<Writeable) | 1;

	uint32_t cr3_val = (((uint32_t)page_directory) & PD_12_Mask) | (1<<4); //disable caching of page directory
	//Setting CR3 register again to force TLB Flush and set page directory again
	asm volatile (	"				\n\
					MOV %0, %%cr3	\n\
					"
					: /* no output */
					: "r"(cr3_val)
					);

	//printf("Added 4MB page for process %d\n", process_num);
}









/* Paging Directory Test
 * Checks the initialization of the paging directory
 * input - none
 * output - none
 * Side Effect - Determines whether or not the
 * paging directory is initialized
 */
void paging_direc_test(void){
	if((page_directory[0] & 0x1) & (page_directory[1] & 0x1) & (~page_directory[2] & 0x1))
		printf("paging direc test PASS\n");
	else
		printf("paging direc test FAIL\n");
}
