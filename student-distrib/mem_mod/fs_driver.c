/* File system driver */

#include "fs_driver.h"
#include "../lib.h"
#include "../mem_mod/file_descriptor.h"


#define FILE_RTC  0
#define FILE_DIR  1
#define FILE_FILE 2

#define NAME_SIZE  32
#define BLK_SIZE   4096
#define N_FILES    63
#define LINE_SIZE  32
#define ITOA_SIZE  7

#define A_SIZE 11
#define B_SIZE 13
#define radix  10

static boot_block_t* boot_block_ptr; 	//Pointer to BOOT BLOCK
static inode_t* inode_block_ptr; 		//Pointer to 1st INODE BLOCK

static int32_t fs_read(int32_t fd, void* buf, uint32_t nbytes);
static int32_t dir_read(int32_t fd, void* buf, uint32_t nbytes);
static int32_t fs_write();

/* Dispatchers assosciated with FD type = file
 * input - cmd - what to do to File
 *       - arg1, arg2, arg3- arguments for command
 * output - none
 * return value - return from function or -1 is fail
 * side effects - none
 */
int32_t file_dispatch(uint32_t cmd, void * arg1, void * arg2, void * arg3){
	switch (cmd) {
		case OPEN:
			return 0;
		case CLOSE:
			return 0;
		case READ:
			return fs_read(*( (uint32_t*)arg1 ), arg2, *( (uint32_t*)arg3)); // needs to have args fixed
		case WRITE:
			return fs_write();
		default:
			return -1;
	}
}

/* Dispatchers assosciated with FD type = directory
 * input - cmd - what to do to File
 *       - arg1, arg2, arg3- arguments for command
 * output - none
 * return value - return from function or -1 is fail
 * side effects - none
 */
int32_t dir_dispatch(uint32_t cmd, void * arg1, void * arg2, void * arg3){
	switch (cmd) {
		case OPEN:
			return 0;
		case CLOSE:
			return 0;
		case READ:
			return dir_read(*( (uint32_t*)arg1 ), arg2, *( (uint32_t*)arg3)); // needs to have args fixed
		case WRITE:
			return fs_write();
		default:
			return -1;
	}
}


/* This function sets boot block pointer and inode block pointer
 * input - boot block pointer from multiboot
 * output - none
 * return value - none
 * sideeffect - changes internal bootblock and inode_block pointers
 */
void init_fs_driver(unsigned int ptr) {
	boot_block_ptr = (boot_block_t*) ptr;
	inode_block_ptr = (inode_t*) (ptr+BLK_SIZE);
}

/* This function returns file size
 * input - filename
 * output - none
 * return value - file size
 * sideeffect - nothing
 */
int32_t get_file_size(const uint8_t* filename){
	dir_entry_t file;
	if(0 == get_dir_entry_by_name(filename, &file)) 
		return inode_block_ptr[file.inode_num].length;

	return -1;
}


/* This function returns a dentry that matches the file name
 *	input - fname - Name of file
 *	ouput - 0 if sucess
 *	return value - -1 if fail, 0 if successful
 *	side effects - changes the dentry* input
 */
int32_t get_dir_entry_by_name(const uint8_t* fname, dir_entry_t* dentry) {
    if (fname == NULL)
        return -1; //means fname is invalid
   
    int i = 0;
    for(i=0; i < (N_FILES-1); i++){ //iterate 63 times
        //temp is a local dentry pointer
				dir_entry_t temp = boot_block_ptr->dir_entries[i];
        const uint8_t* str_temp = (uint8_t*) &(temp.fname);

        if( 0 == strncmp((int8_t*) fname, (int8_t*) str_temp, NAME_SIZE)){
					*dentry = temp; //fname matched
					return 0;
        }
    }
    return -1; //fname didnt match
}

/* This function returns a dentry given the directory entry index in boot block
 * input - offset - offset to start reading from
 *         nbytes - bytes to read
 *         inode - inode of file to read
 *	ouput - buf - bytes containing bytes from file
 *	return value - returns number of bytes read and placed in buffer.
 *                  return -1 on failure
 *	side effects - none
 */
int32_t get_dir_entry_by_index(uint32_t index, dir_entry_t* dentry) {
    if (index > (N_FILES-1)) //exceded index
        return -1;
    else{
        dentry = (dir_entry_t*) &(boot_block_ptr->dir_entries[index]);
        return 0;
    }
}


/* Reads Data from file given inode number
 * input - inode file number
 *       - offset where to start reading from
 *       - buf - empty buffer to be loaded by function
 *		 - nbytes - bytes to be written
 * output - none
 * return value - Returns number of bytes read or 0 if none read
 * side effects - none
 */
int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t nbytes) {
	uint32_t bytes_to_read;
	uint32_t bytes_to_write = 0;
    uint32_t bytes_read = 0; //num of bytes written to buf
    uint32_t skip = (boot_block_ptr->num_inodes + 1); //skip INODEs and BOOT BLOCK in memory
	 
    uint32_t size = inode_block_ptr[inode].length; //File size in Bytes
	if(offset > size) return 0;//end of file reached


    uint32_t datablock = offset / BLK_SIZE; //ith DATA BLOCK to start reading from
    uint32_t offset_in_datablock = offset % BLK_SIZE;//Offset within ith DATA BLOCK
		
	if(inode > (boot_block_ptr->num_inodes)) return -1;
    //This is DATA BLOCK num in file system
    uint32_t data_block_num = inode_block_ptr[inode].data_block_ID[datablock];
    //uint32_t writes = 4096-offset_in_datablock;
	uint32_t bytes_left_in_file = size - offset;
	if(nbytes < bytes_left_in_file) bytes_to_read = nbytes;
	else bytes_to_read = bytes_left_in_file;

	bytes_read = 0;
	while(bytes_to_read > 0){

		if(bytes_left_in_file < BLK_SIZE) bytes_to_write = bytes_left_in_file;
		else bytes_to_write = BLK_SIZE;
		if(nbytes < bytes_to_write) bytes_to_write = nbytes;
		if(bytes_to_read < bytes_to_write) bytes_to_write = bytes_to_read;

        if(bytes_read == 0){

			memcpy((uint8_t*) &buf[bytes_read], (uint8_t*)((uint8_t*)(boot_block_ptr) + (skip + data_block_num)*BLK_SIZE + offset_in_datablock), bytes_to_write);
            datablock++;
        	}
        else{
            data_block_num = inode_block_ptr[inode].data_block_ID[datablock];

            memcpy((uint8_t*) &buf[bytes_read], (uint8_t*)((uint8_t*)(boot_block_ptr) + (skip + data_block_num)*BLK_SIZE), bytes_to_write);
        	datablock++;
        	}

		bytes_read += bytes_to_write;
		bytes_left_in_file -= bytes_to_write;
		bytes_to_read -= bytes_to_write;

    }
	return bytes_read;
}

/* WRITE system call - For 391 this does nothing and returns -1
 * input - file descriptor, buf and num of bytes to write
 * output - nothing
 * return value - -1
 * side effects - none
 */
static int32_t fs_write(){
	return -1;
}


/* Connected to READ system call. Reads Data from file
 * input - nbytes - is num of bytes to read
 *       - fd - file descriptor
 *       - buf - empty buffer whch will be loaded by function
 * output - buf
 * return value - Returns number of bytes read or 0 if none read
 * side effects - none
 */
static int32_t fs_read(int32_t fd, void* buf, uint32_t nbytes){
	//printf("%d\n", fd);
	memset(buf, '\0', nbytes);
	fd_entry_t file = get_fd_entry(fd);
	int bytes_read;
	bytes_read = read_data(file.inode, file.file_position, buf, nbytes);

	if(bytes_read > 0){
		uint32_t pos = file.file_position + bytes_read;
		set_entry_pos(fd, pos);
	}		
	
	return bytes_read;
}

/* Connected to READ system call. Reads Data from folder
 * input - nbytes - is num of bytes to read
 *       - fd - file descriptor
 *       - buf - empty buffer whch will be loaded by function
 * output - buf
 * return value - Returns number of bytes read or 0 if none read
 * side effects - none
*/
static int32_t dir_read(int32_t fd, void* buf, uint32_t nbytes){
		uint32_t line = get_fd_entry(fd).file_position;
		uint32_t num_of_iter = (uint32_t) boot_block_ptr->num_dir_entry;
		
		if (line == num_of_iter) return 0;

		memset(buf, '\0', nbytes);
		uint32_t i = 0;
	
		dir_entry_t* t = (dir_entry_t*) &(boot_block_ptr->dir_entries[line]);
		int8_t* str = (int8_t*)&(t->fname);
		if(strlen(str) < nbytes) nbytes = strlen(str);
		memcpy(  &( ((uint8_t*)buf)[i]), str, nbytes);
		i+= nbytes;
		
		line++;
		set_entry_pos(fd, line);
		return i;
}
