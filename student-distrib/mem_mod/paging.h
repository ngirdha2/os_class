/* Paging.h - This is header file for paging function */

#ifndef _PAGING_H
#define _PAGING_H

#include "../types.h"

/* This function initializes pages. It sets 4kB page for video memory, and 4MB page for kernel*/
void init_paging(void);

/* This function will add page with an entry in page directory and page table*/
void change_process_page(uint32_t process_num);
void change_terminal_page(uint32_t terminal);
int32_t vidmap_page(uint32_t user_addr);

/* check page directory test*/
void paging_direc_test(void);

#endif /* _PAGING_H */
