/* Header file for File System driver */

#ifndef _FS_DRIVER_H
#define _FS_DRIVER_H

#include "../types.h"

void init_fs_driver(unsigned int ptr);


//Routines in File System module

int32_t get_dir_entry_by_name(const uint8_t* fname, dir_entry_t* dentry);
int32_t get_dir_entry_by_index(uint32_t index, dir_entry_t* dentry);
int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length);
int32_t get_file_size(const uint8_t* filename);



#endif
