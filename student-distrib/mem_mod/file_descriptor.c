#include "file_descriptor.h"
#include "../lib.h"

#define KERNEL_END        0x800000  //8MB
#define PROGRAM_BASE     0x8000000  //128MB - 1st Process Base
#define PROCESS_OFFSET     0x48000
#define PAGE_SIZE         0x400000   //4 MB
#define KS_SIZE             0x2000   //8KB



//static pcb_t pcb;
extern int32_t rtc_dispatch(uint32_t cmd, void *arg1, void *arg2, void *arg3);
extern int32_t dir_dispatch(uint32_t cmd, void *arg1, void *arg2, void *arg3);
extern int32_t file_dispatch(uint32_t cmd, void *arg1, void *arg2, void *arg3);

extern pcb_t* get_curr_pcb();


/* Adds a file to the fd array
 * inputs -
 * 		file d_entry
 * outputs - the fd of the added file
 * Side effect - modifies fd array
 */
int32_t add_fd_entry(dir_entry_t file){
	pcb_t* pcb;
	if((pcb = get_curr_pcb()) == NULL) return -1; //Get PCB of current process which is to be halted

	int fd;
	for(fd = 2; fd < FD_SIZE; fd++){
		if(!pcb->fd_array[fd].flag){
			switch (file.ftype) {
				case 0:
					pcb->fd_array[fd].dispatch = rtc_dispatch;
					break;
				case 1:
					pcb->fd_array[fd].dispatch = dir_dispatch;
					break;
				case 2:
					pcb->fd_array[fd].dispatch = file_dispatch;
					break;
				default:
					//printf("UNKNOWN FILETYPE\n");
					return -1;
			}

			pcb->fd_array[fd].inode = file.inode_num;
			pcb->fd_array[fd].file_position = 0;
			pcb->fd_array[fd].flag = TRUE;
			return fd;
		}
	}
	//printf("CANT ADD FILE, TOO MANY OPEN FILES\n");
	return -1;
}



/* Removes a file from the fd array
 * inputs -
 * 		file descriptor
 * outputs - 0 on sucess -1 on failure
 * Side effect - modifies fd array
 */
int32_t remove_entry(uint32_t fd){
	pcb_t* pcb;
	if((pcb = get_curr_pcb()) == 0) return -1; //Get PCB of current process which is to be halted
	if (fd == STDIN_FD || fd == STDOUT_FD) return -1;
	pcb->fd_array[fd].dispatch = NULL;
	pcb->fd_array[fd].inode = 0;
	pcb->fd_array[fd].file_position = 0;
	pcb->fd_array[fd].flag = FALSE;
	return 0;
}



/* Returns a copy of an fd_entry
 * inputs -
 * 		file descriptor
 * outputs - fd_entry
 * Side effect - none
 */
fd_entry_t get_fd_entry(uint32_t fd){
	pcb_t* pcb;
	pcb = get_curr_pcb(); //Get PCB of current process which is to be halted
	return pcb->fd_array[fd];
}



/* Sets the file position of an fd_entry
 * inputs -
 * 		file descriptor
 * outputs - none
 * Side effect - modifies fd_entry position
 */
void set_entry_pos(uint32_t fd, uint32_t pos){
	pcb_t* pcb;
	pcb = get_curr_pcb();
	pcb->fd_array[fd].file_position = pos;
}



/* Prints the files in use by the current pcb
 * inputs - none
 * outputs - none
 * Side effect - modifies fd_entry position
 */
void print_active_files(){
	pcb_t* pcb;
	if((pcb = get_curr_pcb()) == 0) return; //Get PCB of current process which is to be halted
	int fd;
	for(fd = 2; fd < FD_SIZE; fd++){
		if(!pcb->fd_array[fd].flag)
			printf("%d\n", pcb->fd_array[fd].inode);
	}
}
