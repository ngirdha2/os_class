#ifndef FILE_DESCRIPTOR_H
#define FILE_DESCRIPTOR_H

#include "../types.h"


// Initialize the file descriptor array
int32_t add_fd_entry(dir_entry_t ptr);
int32_t remove_entry(uint32_t fd);
fd_entry_t get_fd_entry(uint32_t fd);
void set_entry_pos(uint32_t fd, uint32_t pos);
void print_active_files();

#endif
