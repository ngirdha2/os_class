#include "tests.h"
#include "x86_desc.h"
#include "lib.h"


#include "syscall_mod/rtc.h"
#include "syscall_mod/syscall.h"

#include "interrupt_mod/keyboard_driver.h"

#include "mem_mod/fs_driver.h"
#include "mem_mod/paging.h"
#include "mem_mod/file_descriptor.h"

#define PASS 1
#define FAIL 0

//#define PROCESS_BASE 0x08000000

/* format these macros as you see fit */
#define TEST_HEADER 	\
	printf("[TEST %s] Running %s at %s:%d\n", __FUNCTION__, __FUNCTION__, __FILE__, __LINE__)
#define TEST_OUTPUT(name, result)	\
	printf("[TEST %s] Result = %s\n", name, (result) ? "PASS" : "FAIL");

static inline void assertion_failure(){
	/* Use exception #15 for assertions, otherwise
	   reserved by Intel */
	asm volatile("int $15");
}


/* Checkpoint 1 tests */

/* IDT Test - Example
 *
 * Asserts that all IDT entries are not NULL
 * Inputs: None
 * Outputs: PASS/FAIL
 * Side Effects: None
 * Coverage: Load IDT, IDT definition
 * Files: x86_desc.h/S
 */
int idt_test(){
	TEST_HEADER;

	int i;
	int result = PASS;
	for (i = 0; i < NUM_VEC; ++i){
		if (((idt[i].offset_15_00 == NULL) &&
			(idt[i].offset_31_16 == NULL)) || (idt[i].seg_selector != KERNEL_CS) || (idt[i].present != 1)){
			assertion_failure();
			result = FAIL;
		}
	}

	return result;
}

/*int gdt_test(){
	TEST_HEADER;

	int i;
	int result = PASS;
	for (i = 0; i < NUM_VEC; ++i){
		if ((gdt_ptr. == NULL) &&
			(idt[i].offset_31_16 == NULL)){
			assertion_failure();
			result = FAIL;
		}
	}

	return result;
}*/

/* Paging test
 * We try to dereference a NULL pointer in this test.
 * With paging enabled this function should generate page fault,
 * otherwise it should print value at 0x0
 * input - none
 * output - none
 * Side Effect - none
 */
int paging_test(){
	TEST_HEADER;

	int* temp = NULL;
	printf("Printing value at address 0x0 %d\n",*temp);
	return PASS;
}

/* RTC Test
 *
 * This test is testing the RTC interrupt functionality
 * by using the function interrupt test in lib.c
 * input - none
 * output - none
 * Side Effect - Determines whether or not RTC interrupts is working
 *
 */
int rtc_test(){
	TEST_HEADER;

	test_interrupts();
	return PASS;
}

/* Checkpoint 2 tests */
/* Terminal Test
 *
 * This test is testing the Terminal Driver functionality
 * input - none
 * output - none
 * Side Effect - Echoes keyboard commands upon carriage return
 *
 */
int terminal_test(){
	TEST_HEADER;
	uint8_t buf[128];
	//terminal_open();
	close(STDIN_FD);
	for(;;){
		//read(STDIN_FD, buf, 128);
		//write(STDOUT_FD, buf, 128);
		read(STDIN_FD, buf, 128);
		write(STDOUT_FD, buf, 128);
	}
	close(STDIN_FD);
	close(STDOUT_FD);
	return PASS;
}

/* RTC Frequency Test
 *
 * This test is testing the Terminal Driver functionality
 * input - none
 * output - none
 * Side Effect - Echoes keyboard commands upon carriage return
 *
 */
/*int rtc_freq_test(){
	TEST_HEADER;

	int i;
    uint8_t file1[32] = "rtc\0";
	int fd = open(file1);
	rtc_write(2);
	for(i = 0; i < 20; i++){
		rtc_read(); printf("1"); }
	rtc_write(4);
	for(i = 0; i < 20; i++){
		rtc_read(); printf("1"); }
	rtc_write(8);
	for(i = 0; i < 20; i++){
		rtc_read(); printf("1"); }
	rtc_write(16);
	for(i = 0; i < 20; i++){
		rtc_read(); printf("1"); }
	rtc_write(32);
	for(i = 0; i < 20; i++){
		rtc_read(); printf("1"); }
	close(fd);
	return PASS;
}*/

/* File System Test - frame1
 *
 * This tests reading under 4kB file
 * input - none
 * output - none
 * Side Effect - none
 */
int file_system_test_frame1(){
	TEST_HEADER;

	uint8_t file1[32] = "frame1.txt\0";
	int fd = -1;
	fd = open(file1);
	uint8_t buf[174] = {0};
	read(fd,&buf,174);
	int i=0;
	for(i = 0; i<174; i++){
		printf("%c", buf[i]);
	}
	while(buf[172] != '-'){
		printf("FS_FRAME1_TEST FAILED\n");
	}
	close(fd);

	return PASS;
}

/* File System Test - cat
 *
 * This tests the FileSystem functionality
 * input - none
 * output - none
 * Side Effect - none
 */
int file_system_test_cat(){
	TEST_HEADER;

	uint8_t file1[32] = "cat\0";
	int fd = open(file1);
	uint8_t buf[5445] = {0};
	read(fd,&buf,5445);
	int i=0;
	for(i = 0; i<5445; i++){
		printf("%c", buf[i]);
	}
	while(buf[5443] != 'Z'){
		printf("FS_CAT_TEST FAILED\n");
	}
	close(fd);

	return PASS;
}

/* File System Test - fish
 *
 * This tests reading over 4kB file
 * input - none
 * output - none
 * Side Effect - none
 */
int file_system_test_fish(){
	TEST_HEADER;

	uint8_t file1[32] = "fish\0";
	int fd = open(file1);
	uint8_t buf[36164] = {0};
	read(fd,&buf,36164);
	int i=0, k=0;
	for(i = 0; i<36164; i++){
		if(buf[i] == 'r')
			k++;
	}
	while(k != 3){
		printf("FS_FISH_TEST broke\n");
	}
	close(fd);

	return PASS;
}

/* File System Test - long
 *
 * This tests reading long file name
 * input - none
 * output - none
 * Side Effect - none
 */
int file_system_test_long(){
	TEST_HEADER;

	uint8_t file1[32] = "verylargetextwithverylongname.tx";
	int fd = open(file1);
	uint8_t buf[5277]={0};
	read(fd,&buf, 5277);
	int i=0;
	for(i = 0; i<5277; i++){
		printf("%c", buf[i]);
	}
	while(buf[5275] != '?'){
		printf("FS_LONG_TEST FAILED\n");
	}
	close(fd);

	return PASS;
}

/* File System Test - counter
 *
 * This tests the FileSystem Driver functionality
 * input - none
 * output - none
 * Side Effect - none
 */
int file_system_test_counter(){
	TEST_HEADER;

	uint8_t file1[32] = "counter";
	int fd = open(file1);
	uint8_t buf[5605]={0};
	read(fd,&buf, 5605);
	int i=0;
	for(i = 0; i<5605; i++){
		printf("%c", buf[i]);
	}
	while(buf[5603] != 'Z'){
		printf("FS_COUNTER_TEST FAILED\n");
	}
	close(fd);

	return PASS;
}

/* File System Test - dir
 *
 * This tests Directory File type
 * input - none
 * output - none
 * Side Effect - none
 */
int file_system_test_dir(){
	TEST_HEADER;

	uint8_t file1[32] = ".\0";
	int fd = open(file1);
	uint8_t buf[80]={0};
	read(fd,&buf,80);
	int i=0;
	for(i = 0; i<80; i++){
		printf("%c", buf[i]);
	}
	while( read(fd, &buf, 80) != 0){
		for(i = 0; i<80; i++){
			printf("%c", buf[i]);
		}
	}
	while(buf[72] != '5' || buf[73] != '3' || buf[74] != '4' || buf[75] != '9'){
		printf("%c FS_DIR_TEST FAILED\n", buf[76]);
	}
	close(fd);

	return PASS;
}

/* File System Test - buffer_size
 *
 * This checks that part of regular file is only read until buffer is full
 * input - none
 * output - none
 * Side Effect - none
 */
int file_system_test_buffer_size(){
	TEST_HEADER;

	uint8_t file1[32] = "counter";
	int fd = open(file1);
	uint8_t buf1[5000]={0};
	read(fd,&buf1, 5000);
	close(fd);
	//Page Fault if buffer overflow
	uint8_t file2[32] = "frame0.txt";
	fd = open(file2);
	uint8_t buf2[80]={0};
	read(fd,&buf2, 80);
	close(fd);

	return PASS;
}

/* File System Test - rtc
 *
 * This tests reading RTC file type
 * input - none
 * output - none
 * Side Effect - none
 */
/*int file_system_test_rtc(){
	TEST_HEADER;

	uint8_t file1[32] = "rtc\0";
	int fd = open(file1);
	rtc_open();
	uint8_t buf[80]={0};
	printf("Address of buf = %d\n", &buf[0]);
	read(fd,&buf,80);//36164);
	int i=0;
	for(i = 0; i<80; i++){
		printf("%c", buf[i]);
	}
	close(fd);
	rtc_close();

	return PASS;
}*/


/* File System Test
 *
 * This tests reading over 4kB file
 * input - none
 * output - none
 * Side Effect - none
 */
int read_file_test(){
	TEST_HEADER;

	uint8_t filename[128];
	int32_t file_size;

	int fd;
	fd = -1;

	for(;;){


		clear_buf();

		do{
			read(STDIN_FD, filename, 128);
		}
		while((fd = open(filename)) < 0);

		if((file_size = get_file_size(filename)) < 0) continue;
		uint8_t buf[file_size];
		memset(buf, '\0', file_size);

		if(write(STDOUT_FD, filename, 128) < 0) continue;
		printf("FILESIZE %d\n", file_size);
		//if((fd = open(filename)) < 0) continue;

		//for when reading with offsets is enabled
		//while(read(fd,&buf,25) > 0)
		//	if(write(STDOUT_FD, &buf, 0) < 0) return FAIL;
		if(read(fd, buf, file_size) < 0) continue;
		if(write(STDOUT_FD, buf, file_size) < 0) continue;
		if(close(fd) < 0) continue;
		printf("\n");

/*
		filename = (uint8_t*)"frame0.txt";
		get_dir_entry_by_name(filename, &file);

		//pcb_t* temp = get_curr_pcb();
		//temp->fd_array[0].inode = 2;

		int fd;
		fd = add_fd_entry(file);
		print_active_files();
		//void* buff;
		file_size = get_file_size(command);
		uint8_t buff[file_size];
		memset(buff, '\0', file_size);
		fd_entry_t entry = get_fd_entry(fd);//
		entry.dispatch(READ, &fd, buff, &file_size);


		printf("%s\n", buff);

		remove_entry(fd);
*/
	}
	return PASS;
}

/* RTC Frequency Test
 *
 * This test is testing the Terminal Driver functionality
 * input - none
 * output - none
 * Side Effect - Echoes keyboard commands upon carriage return
 *
 */
int read_rtc_test(){
	TEST_HEADER;

	uint8_t filename[128] = "rtc";
	int fd;
	fd = -1;

	if((fd = open((uint8_t *)&filename)) < 0)
		return FAIL;

	//for when reading with offsets is enabled
	/*while(read(fd,&buf,25) > 0)
		if(write(STDOUT_FD, &buf, 0) < 0) return FAIL;*/
	int i, j;
	for(i = 1; i < 2048; i = i * 2){
		write(fd, &i, 0);
		for(j = 0; j < 10; j++){
			read(fd, &i, 186);
			printf("1");
		}
		printf("\n");
	}

	if(close(fd) < 0) return FAIL;
	return PASS;
}


/* File System Test - dir
 *
 * This tests Directory File type
 * input - none
 * output - none
 * Side Effect - none
 */
int read_dir_test(){
	TEST_HEADER;

	int fd;
	uint8_t file1[32] = ".\0";
	fd = open(file1);
	uint8_t buf[80]={0};
	read(fd,&buf,80);
	int i=0;
	for(i = 0; i<80; i++){
		printf("%c", buf[i]);
	}
	while( read(fd, &buf, 80) != 0){
		for(i = 0; i<80; i++){
			printf("%c", buf[i]);
		}
	}
	while(buf[72] != '5' || buf[73] != '3' || buf[74] != '4' || buf[75] != '9'){
		printf("%c FS_DIR_TEST FAILED\n", buf[76]);
	}
	close(fd);

	return PASS;
}

/* Checkpoint 3 tests */
/* Check if 128MB+4MB is readable after paging
 * input - none
 * output - none
 * Side Effect - Adds a page for process ID 0
 */
int CK3_add_page_test(){
	TEST_HEADER;
	change_process_page(0);
	int* temp = (int32_t*) 0x08000004; //access 128MB + 4B
	printf("%d", *temp);
	return PASS;
}

/* Checkpoint 4 tests */
/* Checkpoint 5 tests */


/* Test suite entry point */
void launch_tests(){

	//CK1 tests
	//TEST_OUTPUT("paging_test", paging_test());
	//TEST_OUTPUT("idt_test", idt_test());
	//CK2 tests
	TEST_OUTPUT("FS_test_frame1", file_system_test_frame1());
	TEST_OUTPUT("FS_test_cat", file_system_test_cat());
	TEST_OUTPUT("FS_test_fish", file_system_test_fish());
	TEST_OUTPUT("FS_test_long", file_system_test_long());
	TEST_OUTPUT("FS_test_counter", file_system_test_counter());
	//TEST_OUTPUT("FS_test_dir", file_system_test_dir());
	TEST_OUTPUT("FS_buffer_test", file_system_test_buffer_size());
	//TEST_OUTPUT("FS_test_rtc", file_sytem_test_rtc());
	//TEST_OUTPUT("rtc_test", rtc_test()); // Tests the rtc interrupt handler

	//CK3 tests
	change_process_page(0);
	//init_pcb((uint32_t*)PROCESS_BASE);
	TEST_OUTPUT("CH3_page_test", CK3_add_page_test());
	//TEST_OUTPUT("rtc_freq_test", rtc_freq_test());
	//TEST_OUTPUT("rtc_test", rtc_test());
	//TEST_OUTPUT("terminal_test", terminal_test());


	//TEST_OUTPUT("read_dir_test", read_dir_test());
	//printf("\n");
	//TEST_OUTPUT("read_rtc_test", read_rtc_test());
	//printf("\n");
	//TEST_OUTPUT("read_file_test", read_file_test());

	printf("ALL TESTS PASSED\n");
}
