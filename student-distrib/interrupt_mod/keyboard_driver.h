#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "../types.h"

void KEYBOARD_INTERRUPT();

int32_t remove_terminal(uint32_t terminal_ID);
int32_t activate_terminal(uint32_t terminal_ID);
void set_terminal_top_process(pcb_t* process);
terminal_t* get_active_terminal();

void clear_buf();

#endif
