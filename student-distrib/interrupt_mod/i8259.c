/* i8259.c - Functions to interact with the 8259 interrupt controller
 * vim:ts=4 noexpandtab
 */

#include "i8259.h"
#include "../lib.h"

#define  NUM_PORTS 8
#define  PORT_MAX  15

/* Interrupt masks to determine which interrupts are enabled and disabled */
uint8_t master_mask; /* IRQs 0-7  */
uint8_t slave_mask;  /* IRQs 8-15 */

/* Pause for after writing to a port */
#define pause()                         \
do {                                    \
    asm volatile ("rep; nop"            \
              :                         \
              :                         \
              : "memory", "cc"          \
    );                                  \
} while (0)

/* i8259_init
 *
 * Initializes the PIC for the OS
 * Inputs: None
 * Outputs: None
 * Side Effects: Initializes the PIC to the given ports and data ports
 *
 */
void i8259_init(void) {

  outb(0xff, MASTER_8259_PORT_DATA);
  outb(0xff, SLAVE_8259_PORT_DATA);

  outb(ICW1, MASTER_8259_PORT);
  pause();
  outb(ICW1, SLAVE_8259_PORT);
  pause();

  outb(ICW2_MASTER, MASTER_8259_PORT_DATA);
  pause();
  outb(ICW2_SLAVE, SLAVE_8259_PORT_DATA);
  pause();

  outb(ICW3_MASTER, MASTER_8259_PORT_DATA);
  pause();
  outb(ICW3_SLAVE, SLAVE_8259_PORT_DATA);
  pause();

  outb(ICW4, MASTER_8259_PORT_DATA);
  pause();
  outb(ICW4, SLAVE_8259_PORT_DATA);
  pause();

  // Initialize all ports to unused except for port 2
  outb(0xfb, MASTER_8259_PORT_DATA);
  outb(0xff, SLAVE_8259_PORT_DATA);
  pause();
  pause();
}

/* enable_irq
 *
 * Enable (unmask) the specified IRQ
 * Inputs: The interrupt number
 * Outputs: None
 * Side Effects: Enables the specified port on the PIC
 *
 */
void enable_irq(uint32_t irq_num) {
  uint16_t port;
  uint8_t value;
  // Sets the port specified by the argument
  if (irq_num < NUM_PORTS)
    port = MASTER_8259_PORT_DATA;
  else{
    irq_num -= NUM_PORTS;
    port = SLAVE_8259_PORT_DATA;
  }
  // Unmask the port of the master or slave
  value = inb(port) & ~(1 << irq_num);
  outb(value, port);
}

/* disable_irq
 *
 * Disable (mask) the specified IRQ
 * Inputs: The interrupt number
 * Outputs: None
 * Side Effects: Disables the specified port on the PIC
 *
 */
void disable_irq(uint32_t irq_num) {
  uint16_t port;
  uint8_t value;
  // Sets the port specified by the argument
  if (irq_num < NUM_PORTS)
    port = MASTER_8259_PORT_DATA;
  else{
    irq_num -= NUM_PORTS;
    port = SLAVE_8259_PORT_DATA;
  }
  // Mask the port of the master or slave
  value = inb(port) | (1 << irq_num);
  outb(value, port);
}

/* send_eoi
 *
 * Send end-of-interrupt signal for the specified IRQ
 * Inputs: The interrupt number
 * Outputs: None
 * Side Effects: Waits for the interrupt at the specified
 * port to be done and then returns
 *
 */
void send_eoi(uint32_t irq_num) {
  // Makes sure the argument is within range
  if(irq_num > PORT_MAX) return;
  else if(irq_num >= NUM_PORTS){ // For the Slave
    outb(EOI | (irq_num - NUM_PORTS), SLAVE_8259_PORT);
    outb(EOI | 2, MASTER_8259_PORT);  //must send
  }
  else // For the Master
    outb(EOI | irq_num, MASTER_8259_PORT);
}
