#include "i8259.h"
#include "keyboard_driver.h"
#include "../lib.h"

// Define macros
#define  KEYBOARD_PORT 0x60
#define  RTC_PIC_POS 8
#define  KEYBOARD_PIC_POS 1
#define  MAX_CHAR_SIZE 128
#define  NUM_TERMINALS 3

#define L_SHIFT_DOWN 42
#define R_SHIFT_DOWN 54
#define L_SHIFT_UP -74
#define R_SHIFT_UP -86
#define CONTROL_UP 29
#define CONTROL_DOWN -99
#define ALT_DOWN 56
#define ALT_UP -72
#define F1_UP -69
#define F1_DOWN 59
#define F2_UP -68
#define F2_DOWN 60
#define F3_UP -67
#define F3_DOWN 61
#define CAPS_LOCK 58
#define BACK_SPACE 14
//#define LEVEL2 0xE0 
#define ESC 0x01
#define UP_PRESS 0x48 //Not differentiating between keyboard UP and cursor UP
//#define UP_RELEASE 0xC8
#define DN_PRESS 0x50 //Not differentiating between keyboard DOWN and cursor DOWN
//#define DN_RELEASE 0xD0
#define ENTER 28
#define CAPS_LETTER_OFFSET 32
#define MAX_BUF_SIZE 128

#define BUF_SIZE 128


/* CRT controller registers */
#define HIGH_ADDRESS 0x0C
#define LOW_ADDRESS  0x0D
#define CRTC_INDEX    0x03d4
#define CRTC_DATA     0x03d5
#define VGA_ACCESS_PAGE_SIZE     0x800
#define INPUT_STATUS_1  0x03DA
#define VRETRACE  0x08


// Initialize file global variables

static int active_terminal = 0;
static int displayed_terminal = -1;

static terminal_t terminals[NUM_TERMINALS];

static bool_t is_shifted = FALSE;
static bool_t is_control = FALSE;
static bool_t is_alt = FALSE;
//static bool_t intr_2_lvl = FALSE; Detect 2 level interrupt responses
static bool_t reverse_search_mode = FALSE;
static bool_t is_F1 = FALSE;
static bool_t is_F2 = FALSE;
static bool_t is_F3 = FALSE;

static int32_t terminal_open();
static int32_t terminal_close();
static int32_t terminal_read(uint8_t* out, uint32_t nbytes);
static int32_t terminal_write(const uint8_t* buf, uint32_t nbytes);
//static void print_terms();
static void system_keyboard_int();
static bool_t get_buf_status();
static int32_t get_buf(uint8_t* buf);


extern uint32_t system_halt(pcb_t* process);
extern uint32_t system_execute(const uint8_t* command, pcb_t* parent_process, uint32_t terminal_ID);
extern void change_terminal_page(uint32_t terminal);
//extern pcb_t* get_scheduled_process();
extern pcb_t* get_curr_pcb();

static void display_terminal(uint32_t terminal);

static uint8_t colors[9] = {0x0A, 0x07, 0x70, 0x0A, 0x07, 0x70,  0x0A, 0x07, 0x70};

// Statically declared array for the keyboard mapping
static char scancode[128] =
{
        0,  27, '1', '2', '3', '4', '5', '6', '7', '8',/* 9 */
        '9', '0', '-', '=',
        '\b', /* Backspace */
        '\t', /* Tab */
        'q', 'w', 'e', 'r', /* 19 */
        't', 'y', 'u', 'i', 'o', 'p', '[', ']',
        '\n', /* Enter key */
        0, /* 29   - Control */
        'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', /* 39 */
        '\'', '`',
        0xE, /* Left shift 42*/
        '\\', 'z', 'x', 'c', 'v', 'b', 'n', /* 49 */
        'm', ',', '.', '/',
        0xE,  /* Right shift 54*/
        '*',
        0, /* Alt */
        ' ', /* Space bar */
        0, /* Caps lock */
        0, /* 59 - F1 key ... > */
        0,   0,   0,   0,   0,   0,   0,   0,
        0, /* < ... F10 */
        0, /* 69 - Num lock*/
        0, /* Scroll Lock */
        0, /* Home key */
        0, /* Up Arrow */
        0, /* Page Up */
        '-',
        0, /* Left Arrow */
        0,
        0, /* Right Arrow */
        '+',
        0, /* 79 - End key*/
        0, /* Down Arrow */
        0, /* Page Down */
        0, /* Insert Key */
        0, /* Delete Key */
        0,   0,   0,
        0, /* F11 Key */
        0, /* F12 Key */
        0, /* All other keys are undefined */
};

// This array is used to get the char that corresponds to the given key press when shift or caps lock is held
static char shifted[128] =
{
        0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31, /*dont care about shifting these */
        32,33,34,35,36,37,38, /*dont care about shifting these */
        34, // ' -> "
        40,41,42,43, // dont care
        60, // , -> <
        95, // - -> _
        62, // . -> >
        63, // / -> ?
        41, // 0 -> )
        33, // 1 -> !
        64, // 2 -> @
        35, // 3 -> #
        36, // 4 -> $
        37, // 5 -> %
        94, // 6 -> ^
        38, // 7 -> &
        42, // 8 -> *
        40, // 9 -> (
        58, //dont care
        58, // ; -> :
        60, // dont care
        43, // = -> +
        62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90, // dont care
        123, // [ -> {
        124, // \ -> |
        125, // ] -> }
        94, 95, //dont care
        126, // ` -> ~
        65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90, // lowercase -> uppercase
        123,124,125,126,127 // dont care
};


/* KEYBOARD_INTERRUPT
 *
 * Reads value from port and prints character to screen
 * Inputs: None
 * Outputs: None
 * Side Effects: Character printed to screen
 *
 */
void KEYBOARD_INTERRUPT(){
        change_terminal_page(displayed_terminal);

        system_keyboard_int();

        change_terminal_page(active_terminal);
}

/* Processing keyboard interrupt
 * input - gets a character input from keyboard port
 * output - none
 * return value - none
 * sideeffects - none
 */
static void system_keyboard_int(){

        terminal_t* terminal = &terminals[displayed_terminal];
        char c = inb(KEYBOARD_PORT);

    if(c == ENTER) {
           terminal->buf_status = TRUE;
           terminal->keyboard_buf[MAX_BUF_SIZE - 1] = '\0';
		   
		if(reverse_search_mode == TRUE){
	       reverse_search_mode = FALSE;
	       memcpy(terminal->keyboard_buf, terminal->found_buf, MAX_BUF_SIZE);
	       terminal->keyboard_buf[terminal->found_buf_size] = '\n';
	       terminal->curr_buf_size = terminal->found_buf_size+1;
        } else {
           terminal->keyboard_buf[terminal->curr_buf_size] = '\n';
           terminal->curr_buf_size++;
        }
        putc('\n');
		
		//Only add to history if A command was typed
		if(terminal->keyboard_buf[0] != '\n'){
			memset( &(terminal->hist[terminal->hist_counter][0]), '\0',  MAX_BUF_SIZE);
			terminal->hist_buf_size[terminal->hist_counter] = terminal->curr_buf_size-1;
			memcpy( &(terminal->hist[terminal->hist_counter][0]), &(terminal->keyboard_buf[0]), MAX_BUF_SIZE);
			terminal->hist_counter = (terminal->hist_counter+1)%HIST_SIZE ; //Increment counter but still limiting to under 10
			terminals->hist_cnt_read = terminal->hist_counter;
		}

		//If "history" was typed then print the history buffers 
		if( terminal->curr_buf_size == 8 ){
			if( 0 == strncmp((const int8_t*)terminal->keyboard_buf, (const int8_t*) "history", 7)){
				//printf("Current history\n");
				int i, j;
				for(i = 0; i < HIST_SIZE; i++){
				for( j = 0; j < terminal->hist_buf_size[i]; j++){
					putc(terminal->hist[i][j]);
				}
				printf("\n"); //printf("<--END\n");
				}
			}
		}
   
		send_eoi(KEYBOARD_PIC_POS);
		return;
    }
        else if(c == L_SHIFT_DOWN || c == R_SHIFT_DOWN || c == L_SHIFT_UP || c == R_SHIFT_UP) {
                is_shifted = !is_shifted;
                send_eoi(KEYBOARD_PIC_POS);
                return;
        }
        //caps lock
        else if(c == CAPS_LOCK) {
                is_shifted = !is_shifted;
                send_eoi(KEYBOARD_PIC_POS);
                return;
        }
        else if(c == CONTROL_DOWN || c == CONTROL_UP) {
                is_control = !is_control;
                send_eoi(KEYBOARD_PIC_POS);
                return;
        }
        else if(c == ALT_DOWN || c == ALT_UP) {
                is_alt = !is_alt;
                send_eoi(KEYBOARD_PIC_POS);
                return;
        }
        else if(c == F1_UP) {
                is_F1 = FALSE;
                send_eoi(KEYBOARD_PIC_POS);
                return;
        }
        else if(c == F1_DOWN) {
                is_F1 = TRUE;
                send_eoi(KEYBOARD_PIC_POS);
                if(is_alt && (displayed_terminal != 0)) activate_terminal(0);
                return;
        }
        else if(c == F2_UP) {
                is_F2 = FALSE;
                send_eoi(KEYBOARD_PIC_POS);
                return;
        }
        else if(c == F2_DOWN) {
                is_F2 = TRUE;
                send_eoi(KEYBOARD_PIC_POS);
                if(is_alt && (displayed_terminal != 1)) activate_terminal(1);
                return;
        }
        else if(c == F3_UP) {
                is_F3 = FALSE;
                send_eoi(KEYBOARD_PIC_POS);
                return;
        }
        else if(c == F3_DOWN) {
                is_F3 = TRUE;
                send_eoi(KEYBOARD_PIC_POS);
                if(is_alt && (displayed_terminal != 2)) activate_terminal(2);
                return;
        }
	else if(c == ESC){
		if(reverse_search_mode){
			reverse_search_mode = FALSE;
			int j;
			for(j=0; j < (22+terminal->cnt+terminal->found_buf_size); j++)
				put_bkspc();
			printf("391OS> ");
			//Replace local variables with idx_fnd values. Then display
			terminal->curr_buf_size = terminal->found_buf_size; //terminal->hist_buf_size[(terminal->idx_fnd)];
			memset(terminal->keyboard_buf, '\0', MAX_BUF_SIZE);
			memcpy(terminal->keyboard_buf, terminal->found_buf, MAX_BUF_SIZE);  
			for(j=0; j < terminal->curr_buf_size ; j++){
				putc(terminal->keyboard_buf[j]);
			}
		}
    send_eoi(KEYBOARD_PIC_POS);
    return;  
   }
   else if(c == BACK_SPACE) {
		if(terminal->curr_buf_size > 0){
			put_bkspc();
			int j;
			if(reverse_search_mode){
				terminal->cnt--;
			for(j=0; j < (3+terminal->found_buf_size); j++) //Delete matched string
			  put_bkspc();
			}
			terminal->keyboard_buf[terminal->curr_buf_size] = '\0';
			terminal->curr_buf_size--;
	  
			if(reverse_search_mode){
				printf("': ");
				printf("%s", terminal->found_buf);
		  /*for( j = 0; j < (terminal->curr_buf_size) ; j++){
			  putc(terminal->keyboard_buf[j]);
		}*/  
	  }  
    }

     send_eoi(KEYBOARD_PIC_POS);
     return;
   }
   else if(c == UP_PRESS){
	//printf("Counter read %d", terminal->hist_cnt_read);
	unsigned int new_cnt = (terminals->hist_cnt_read-1+10)%HIST_SIZE;
	if( new_cnt == terminal->hist_counter){ //detect if looping
		send_eoi(KEYBOARD_PIC_POS);
		return;
	}
	else
		terminals->hist_cnt_read = new_cnt;
	//Clear the current command without saving it
	while(terminal->curr_buf_size > 0){
      put_bkspc();
      terminal->keyboard_buf[terminal->curr_buf_size] = '\0';
      terminal->curr_buf_size--;
    }
	int j;
	//Print previous command
	for( j = 0; j < terminal->hist_buf_size[terminals->hist_cnt_read]; j++ ){
		c = terminal->hist[terminals->hist_cnt_read][j];
		terminal->keyboard_buf[terminal->curr_buf_size] = c;
		putc(c);
		terminal->curr_buf_size++;
	}  
	send_eoi(KEYBOARD_PIC_POS);
	return;
  }
  else if(c == DN_PRESS){
	//printf("Counter read %d", terminal->hist_cnt_read);
	unsigned int new_cnt = (terminals->hist_cnt_read+1)%HIST_SIZE;
	if (new_cnt == terminal->hist_counter){
		send_eoi(KEYBOARD_PIC_POS);
		return;
	}
	else
		terminals->hist_cnt_read = new_cnt;
	//Clear the current command without saving it
	while(terminal->curr_buf_size > 0){
      put_bkspc();
      terminal->keyboard_buf[terminal->curr_buf_size] = '\0';
      terminal->curr_buf_size--;
    }
	int j;
	for( j = 0; j < terminal->hist_buf_size[terminals->hist_cnt_read]; j++ ){
		c = terminal->hist[terminals->hist_cnt_read][j];
		terminal->keyboard_buf[terminal->curr_buf_size] = c;
		putc(c);
		terminal->curr_buf_size++;
	}  
	send_eoi(KEYBOARD_PIC_POS);
	return;
  }
  else if(terminal->curr_buf_size >= MAX_BUF_SIZE - 3) {
           terminal->curr_buf_size = MAX_BUF_SIZE - 3;
  }
  else if(c > 0) {

          c = scancode[(int)c];
          if(is_shifted)
          c = shifted[(int)c];

          if(is_control) {
                    if(c == 'l' || c == 'L') {
                                clear();
                                clear_buf();
                                send_eoi(KEYBOARD_PIC_POS);


                               // printf("%d_391OS> ", displayed_terminal);

                                return;
                    }
		else if(c == 'r' || c == 'R'){
			clear_buf();
			//Starting reverse search so initialize all variables
			reverse_search_mode = TRUE;
			terminal->cnt = 0;
			terminal->found_buf_size = 0;
			memset(terminal->found_buf, '\0', MAX_BUF_SIZE); 
			int j;
			for(j=0; j<7; j++)
				put_bkspc();
			printf("(reverse-i-search)`': ");
			send_eoi(KEYBOARD_PIC_POS);
			return;
		}
                        else if(c == 'c' || c == 'C') {
                                clear_buf();
                                send_eoi(KEYBOARD_PIC_POS);
                                return;

                                /*
                                printf("\nUSER KEYBOARD HALT %d\n", displayed_terminal);
                                uint32_t ret = system_halt(terminal->top_process);
                                asm volatile ("\
                                          movl %0, %%esp; \
                                          popal;\
                                          movl $0, %%eax; \
                                          leave; \
                                          ret;  \
                                          "
                                              : "=r" (ret)
                                              );*/
                        }
                }

    terminal->keyboard_buf[terminal->curr_buf_size] = c;
    terminal->curr_buf_size++;

	if(reverse_search_mode){
		int j, k, size; //size is number of chars to compare
		for(j=0; j < (3+terminal->found_buf_size); j++)
			put_bkspc();			putc(c);
		terminal->cnt++;
		printf("': ");
		terminal->found_buf_size = 0; 							//Clean up Found buffer
		memset(terminal->found_buf, '\0', MAX_BUF_SIZE);		//Clean up Found buffer
		int left; //no of characters from left already searched
		for( j = 0; j < HIST_SIZE; j++){
		  for( left = 0; left < terminal->hist_buf_size[j]; left++){
		  
			
			/*printf("%s\n",terminal->keyboard_buf);
			  for( k = 0; k < terminal->hist_buf_size[j]; k++){
					putc(terminal->hist[j][k]);
				}*/
			 if( terminal->curr_buf_size > terminal->hist_buf_size[j]-left )
				 size = terminal->hist_buf_size[j]-left;
			 else
				 size = terminal->curr_buf_size;
			 /*uint8_t temp[MAX_BUF_SIZE];
			   for(k=0; k < MAX_BUF_SIZE; k++)
				temp[k] = terminal->hist[j][k];*/
			 if( (size>0) && ( 0 == strncmp((const int8_t*)terminal->keyboard_buf, (const int8_t*)&(terminal->hist[j][0+left]), size)) ){

				terminal->found_buf_size = terminal->hist_buf_size[j];
				for( k = 0; k < (terminal->found_buf_size); k++){
					terminal->found_buf[k] = terminal->hist[j][k];
				}
				printf("%s", terminal->found_buf);
				//for( j = 0; j < (terminal->curr_buf_size); j++)
				//	putc(terminal->keyboard_buf[j]);
				goto nested_loop_exit; //Force to break out of nested loop
				
			}
		  }
		}
	}
	else
        putc(c);

        }
        nested_loop_exit: send_eoi(KEYBOARD_PIC_POS); //dont send
}



//==============TERMINAL SYSCALLS================
/* Dispatchers for IO Open, Close, Read, Write
 * input - cmd argument
			arg1, arg2, arg3 are arguments for Read and Write
 * output - none
 * return value - -1 if fail, 0 for success
 * sideeffects - none
 */
int32_t io_dispatch(uint32_t cmd, void * arg1, void * arg2, void * arg3){
        switch (cmd) {
        case OPEN:
                return terminal_open();
        case CLOSE:
                return terminal_close();
        case READ:
                return terminal_read((uint8_t *) arg2, *((uint32_t*) arg3));
        case WRITE:
                return terminal_write((uint8_t *) arg2, *((uint32_t *)arg3));
        default:
                return -1;
        }
}

/* terminal_open
 *
 * System call open for terminal driver
 * Inputs: None
 * Outputs: Returns 0
 * Side Effects: Initialize a bufer filled with NULL
 *
 */
static int32_t terminal_open(){
        clear_buf();
        return 0;
}

/* terminal_close
 *
 * System call close for terminal driver
 * Inputs: None
 * Outputs: Returns 0
 * Side Effects: Clear the bufer and fill with NULL
 *
 */
static int32_t terminal_close(){

        clear_buf();
        return 0;
}

/* terminal_write
 *
 * System call write for terminal driver
 * Inputs: The bufer and number of bytes to write
 * Outputs: Returns the size of the bufer
 * Side Effects: Prints out the contents of the bufer to the terminal
 *
 */
static int32_t terminal_write(const uint8_t* buf, uint32_t nbytes){
        if(buf == 0) {
                return -1;
        }
        //terminal_t* terminal = &terminals[active_terminal];
        //set_text_attrib(terminal->color);
        //printf("%d_", active_terminal);

        int i;
        for(i = 0; i < nbytes; i++) putc(buf[i]);

        return 0;
}

/* terminal_read
 *
 * System call read for terminal driver
 * Inputs: The bufer and number of bytes to read
 * Outputs: Returns the size of the bufer
 * Side Effects: Receives data from the keyboard and is then stored in the bufer
 *
 */
static int32_t terminal_read(uint8_t* out, uint32_t nbytes){
        cli();
        uint8_t buf[BUF_SIZE];
        volatile bool_t buf_status;
        uint8_t curr_buf_size;
        memset(out, '\0', nbytes);
        

        
        sti();
        for(;;) {
          cli();
          buf_status = get_buf_status();
          if(!buf_status){
            sti();
            continue;
          }
          else break;
          
        } 


        curr_buf_size = get_buf(buf);
        if(curr_buf_size > nbytes) curr_buf_size = nbytes;
        if(curr_buf_size == 0) clear_buf();
        memcpy(out, buf, curr_buf_size);
        clear_buf();


        sti();
        return curr_buf_size;
}




//================IO HELPERS================

/* get_buf_status
 *
 * Function for getting the status of the bufer
 * Inputs: None
 * Outputs: Returns the status of the bufer
 * Side Effects: None
 *
 */
static bool_t get_buf_status(){
        terminal_t* terminal = &terminals[active_terminal];
        return terminal->buf_status;
}

/* clear_buf
 *
 * Function for clearing the bufer
 * Inputs: None
 * Outputs: None
 * Side Effects: Clears the bufer
 *
 */
void clear_buf(){
        cli();
        terminal_t* terminal = &terminals[active_terminal];

        terminal->buf_status = FALSE;

        memset(terminal->keyboard_buf, '\0', MAX_BUF_SIZE);
        terminal->curr_buf_size = 0;

        sti();
}

/* get_buf
 *
 * Function for getting the bufer
 * Inputs: a pointer to a char array
 * Outputs: Returns the bufer size
 * Side Effects: Copies the keyboard bufer into the terminal bufer
 *
 */
static int32_t get_buf(uint8_t* buf){

        terminal_t* terminal = &terminals[active_terminal];
        if(buf == 0) return -1;
        memcpy(buf, terminal->keyboard_buf, terminal->curr_buf_size);
        return terminal->curr_buf_size;
}

/* Setting given terminal to active terminal
 * input - terminal number
 * output - none
 * return value - none
 * sideeffects - none
 */
/*static void print_terms(){
        int i = 0;
        for(i = 0; i < NUM_TERMINALS; i++) {
                printf("%d %s\n", terminals[i].buf_status, terminals[i].keyboard_buf);
        }
}*/

/* Setting given terminal to active terminal
 * input - terminal number
 * output - none
 * return value - none
 * sideeffects - none
 */
void set_active_terminal(uint32_t terminal){
        active_terminal = terminal;
}

/* Setting the top process in terminal struct
 * input - pcb pointer
 * output - none
 * return value - none
 * sideeffects - terminal struct changed
 */
void set_terminal_top_process(pcb_t* process){
        terminals[active_terminal].top_process = process;
}

/* Get top process in current terminal that should be displayed
 * input - none
 * output - none
 * return value - PCB pointer of top process
 * sideeffects - none
 */
pcb_t* get_terminal_top_process(){
        return terminals[active_terminal].top_process;
}

/* Display current terminal using VGA control registers
 * input - terminal number
 * output - none
 * return value - none
 * sideeffects - VGA registers changed
 */
static void display_terminal(uint32_t terminal){

        cli();
        //
        uint16_t visible_page = terminal * VGA_ACCESS_PAGE_SIZE;

        uint16_t high_addr=HIGH_ADDRESS | (visible_page & 0xff00);
        uint16_t low_addr =LOW_ADDRESS  | (visible_page << 8);

        while ((inw(INPUT_STATUS_1) & VRETRACE));
        outw(high_addr, CRTC_INDEX);
        outw(low_addr, CRTC_INDEX);
        while (!(inw(INPUT_STATUS_1) & VRETRACE));


        sti();
}

/* Remove the terminal by setting its values to 0
 * input - terminal number
 * output - none
 * return value - -1 if error, 0 if success
 * sideeffects - terminal removed
 */
int32_t remove_terminal(uint32_t terminal_ID){
        int i;
        if(terminal_ID >= NUM_TERMINALS) return -1;


        terminals[terminal_ID].curr_buf_size = 0;
        terminals[terminal_ID].vid_mem = NULL;
        terminals[terminal_ID].buf_status = FALSE;
        terminals[terminal_ID].awake = FALSE;

        for(i = 0; i < NUM_TERMINALS; i++) {
                if(terminals[i].awake) return activate_terminal(i);
        }

        return activate_terminal(0);
}

/* Gets the terminal for current process from its PCB
 * input - none
 * output - none
 * return value - none
 * sideeffects - none
 */
terminal_t* get_process_terminal(){
        pcb_t* process = get_curr_pcb();
        return &terminals[process->terminal];
}

/* Initilizes the terminal structs
 * input - none
 * output - none
 * return value - none
 * sideeffects - modifies the terminal structs
 */
void init_terminals(){
        int i;
        // later add array to be used for the file keys
        for(i = 0; i < NUM_TERMINALS; i++) {

                terminals[i].curr_buf_size = 0;
                terminals[i].vid_mem = NULL;
                terminals[i].buf_status = FALSE;
                terminals[i].awake = FALSE;
                terminals[i].color = colors[i];

                memset(terminals[i].keyboard_buf, '\0', MAX_BUF_SIZE);
	memset(terminals[i].hist_buf_size, '\0', HIST_SIZE);
	memset(terminals[i].hist, '\0', MAX_BUF_SIZE*HIST_SIZE);

        }
        enable_irq(1);
}

/* This function activates a function when Alt+Key is pressed
 * input - terminal number
 * output - none
 * sideeffects - displayed terminal changed
 * return value - 0 if success, -1 if fail
 */
int32_t activate_terminal(uint32_t terminal_ID){
        if(terminal_ID >= NUM_TERMINALS) return -1;
        if(displayed_terminal == terminal_ID) return -1;

        terminals[displayed_terminal].screen_x = get_x();
        terminals[displayed_terminal].screen_y = get_y();

        displayed_terminal = terminal_ID;

        set_x(terminals[displayed_terminal].screen_x);
        set_y(terminals[displayed_terminal].screen_y);



        move_cursor();

        display_terminal(terminal_ID);


        if(!terminals[terminal_ID].awake) {
                terminals[terminal_ID].curr_buf_size = 0;
                terminals[terminal_ID].vid_mem = NULL;
                terminals[terminal_ID].buf_status = FALSE;
                terminals[terminal_ID].awake = TRUE;

                memset(terminals[terminal_ID].keyboard_buf, '\0', MAX_BUF_SIZE);

                system_execute((uint8_t*)"shell", NULL, terminal_ID);


        }

        return 0;
}
