// interrupt handlers

#include "../lib.h"
#include "PIT_driver.h"
#include "i8259.h"
#include "../mem_mod/file_descriptor.h"
#include "../syscall_mod/task_switch.h"
#include "../mem_mod/paging.h"
#include "../syscall_mod/syscall.h"

#define TIMER_CHIP_PIC_POS 0
#define MAX_PID            6
#define TICK_NUM           0
#define KERNEL_END        0x800000  //8MB
#define PROGRAM_BASE     0x8000000  //128MB - 1st Process Base
#define PROCESS_OFFSET     0x48000
#define PAGE_SIZE         0x400000   //4 MB
#define KS_SIZE             0x2000   //8KB

#define NUM_TERMINALS            3

#define ESP_TO_PCB_MASK   0x7FE000





extern void set_kernel_stack(uint32_t esp);
extern void change_terminal_page(uint32_t terminal);
extern int32_t io_dispatch(uint32_t cmd, void *arg1, void *arg2, void *arg3);
extern void set_terminal_top_process(pcb_t* process);
extern int32_t activate_terminal(uint32_t terminal_ID);
extern void set_active_terminal(uint32_t terminal);


static pcb_t* process_queue[MAX_PID];

static int32_t head_process = -1; //most recent process added
static int32_t tail_process = -1; //first process added

static int32_t curr_PID = -1;
static int32_t prev_PID = -1;

static uint32_t process_count = 0;

/* Gets an available PID from 0 to 5
 * input - none
 * output- none
 * return value - PID
 * Side effect - none
 */
int32_t get_PID(){
        int i;

        for(i = 0; i < MAX_PID; i++) {
                if(process_queue[i] == NULL) {
                        return i;
                }
        }
        printf("STACK OVERFLOW WARNING, CANT ADD PROGRAM\n");
        return -1;
}


/* Gets the PCB for current process from ESP address
 * inputs - none
 * outputs - pointer to PCB
 * return value - PCB pointer
 * Side effect - none
 */
pcb_t* get_curr_pcb(){
        cli();
        uint32_t curr_esp = 0;

        // add a check for if your on the kernel or if your on a kernel stack

        asm volatile ("movl %%esp, %0;"
                      : "=r" (curr_esp) //output
                      );
        //do esp bitmasking
        sti();
        return (pcb_t*)(curr_esp & ESP_TO_PCB_MASK);
}

/* Add a process to the list of processes that will be run by scheduler
 * inputs - terminal number
 * outputs - PCB pointer
 * return value - PCB pointer
 * side effect - none
 */
pcb_t* schedule_process(pcb_t* parent_process, uint32_t terminal_ID){
        int32_t PID;
        pcb_t* pcb;
        uint32_t pcb_addr;
        int i;


        if((PID = get_PID()) == -1) return NULL;

        process_count++;

        //check if those values are safe
        pcb_addr = KERNEL_END - ((PID + 1) * KS_SIZE);
        pcb = (pcb_t *) pcb_addr;

        pcb->PID = PID; //This is PID of new child process
        pcb->pcb_addr = pcb_addr;
        pcb->exec_addr = PROGRAM_BASE + PROCESS_OFFSET;
        pcb->ustack_addr = (PROGRAM_BASE + PAGE_SIZE - 0x4);
        pcb->kstack_addr = KERNEL_END - (PID * KS_SIZE) - 0x4;


        pcb->parent_pcb = parent_process;
        if(parent_process != NULL) {
                parent_process->is_active = FALSE;
                pcb->terminal = parent_process->terminal;

        } else{
                pcb->terminal = terminal_ID;

                //clear();
        }

        set_terminal_top_process(pcb);

        pcb->is_active = TRUE;
        pcb->tick = 1;
        pcb->vidmapped = FALSE;


        //init file descriptor array
        for(i = 0; i < FD_SIZE; i++) {
                pcb->fd_array[i].dispatch = NULL;
                pcb->fd_array[i].inode = 0;
                pcb->fd_array[i].file_position = 0;
                pcb->fd_array[i].flag = FALSE;
        }

        //add stdin and stdout
        pcb->fd_array[STDIN_FD].dispatch = io_dispatch;
        pcb->fd_array[STDIN_FD].flag = TRUE;
        pcb->fd_array[STDOUT_FD].dispatch = io_dispatch;
        pcb->fd_array[STDOUT_FD].flag = TRUE;





        //THE PREV_PID SHOULD BE THE ONE JUST ADDED AND THE CURR SHOULD BE TAILS  


        //link process queue
        pcb->prev = head_process;
        pcb->tick = TICK_NUM;

        process_queue[PID] = pcb;

        if(head_process >= 0) process_queue[head_process]->next = PID; //old head process
        else tail_process = PID;
        head_process = PID;
        process_queue[tail_process]->prev = head_process;
        process_queue[head_process]->next = tail_process; //this is also PID

        prev_PID = head_process;
        curr_PID = tail_process;

        return pcb;
}

/* Removes the process queue
 * input - process number PID
 * output - none
 * return value - 0 if success, -1 if fail 
 * sideefects - removes process from list
 */
int32_t remove_process(uint32_t PID){
        if(PID > MAX_PID) return -1;
        if(process_queue[PID] == NULL) return -1;
        if(!process_queue[PID]->is_active) return -1;

        process_count--;

        process_queue[process_queue[PID]->prev]->next = process_queue[PID]->next;
        process_queue[process_queue[PID]->next]->prev = process_queue[PID]->prev;

        if(head_process == PID && tail_process == PID) head_process = tail_process = -1; //emptying queue
        else if(tail_process == PID) tail_process = process_queue[PID]->next; //if removing the bottom process
        else if(head_process == PID) head_process = process_queue[PID]->prev; //if removing the top process

        //THIS WILL ALWAYS GET FIRED FOR NOW
        //if(curr_PID == PID || tail_process == PID){
          curr_PID = tail_process;
          prev_PID = head_process;
        //}
        //OTHERWISE YOU DONT NEED TO CHANGE CURR OR PREV


        process_queue[PID] = NULL;

        return 0;
}

/* Sets the initial process  variables
 * input - none
 * output - sets up global variables for process queue
 * return value - none
 * sideeffects - changes process queue parameters
 */
void init_process_queue(){
        int i;
        for(i = 0; i < MAX_PID; i++)
                process_queue[i] = NULL;

        head_process = -1;
        tail_process = -1;
        curr_PID = -1;
        prev_PID = -1;
}

/* Func returns process pointer 
 * input - none
 * output - none
 * return value - pcb pointer
 * sideeffects - none
 */
/*pcb_t* get_scheduled_process(){
        return process_queue[prev_PID];
}*/


/* TIMER_CHIP_INTERRUPT
 *
 * Scheduler function which is called every time PIT interrupt fires. It handles the process switching
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints functor name to screen
 *
 */
uint32_t SCHEDULER(uint32_t esp){
        uint32_t ret = esp;
        if(process_count == 0) {
                send_eoi(TIMER_CHIP_PIC_POS);
                return ret;
        }

        if((curr_PID == -1 && prev_PID == -1) || process_count == 0) {
                send_eoi(TIMER_CHIP_PIC_POS);
                return ret;
        }

        if(curr_PID == -1) curr_PID = tail_process;
        if(prev_PID == -1) prev_PID = curr_PID;
        volatile pcb_t* curr_process = process_queue[curr_PID]; //switch to this later
        volatile pcb_t* prev_process = process_queue[prev_PID]; //switch to this later

        prev_process->esp = esp;

        if(!curr_process->is_active) {
                curr_process->tick = TICK_NUM;
        }
        else if((curr_process->tick)-- <= 0) {

                change_terminal_page(curr_process->terminal);
                curr_process->tick = TICK_NUM;
                change_process_page(curr_process->PID);
                set_kernel_stack(curr_process->kstack_addr);
                curr_process->tick = TICK_NUM;
                set_active_terminal(curr_process->terminal);
                ret = curr_process->esp;
        }

        prev_PID = curr_PID;
        curr_PID = process_queue[curr_PID]->next;

        send_eoi(TIMER_CHIP_PIC_POS);
        return ret;
}
