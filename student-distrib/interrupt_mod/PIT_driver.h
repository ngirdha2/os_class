#ifndef PIT_DRIVER_H
#define PIT_DRIVER_H

#ifndef ASM

#include "../types.h"

uint32_t SCHEDULER(uint32_t esp);
pcb_t* schedule_process(pcb_t* parent_process, uint32_t terminal_ID);
int32_t remove_process(uint32_t PID);
pcb_t* get_curr_pcb();
void init_process_queue();


#endif
#endif
