#ifndef INT_WRAPPERS_H
#define INT_WRAPPERS_H

#include "keyboard_driver.h"
#include "PIT_driver.h"

#include "../syscall_mod/rtc.h"

extern void REAL_TIME_CLOCK_INTERRUPT_WRAPPER();
extern void KEYBOARD_INTERRUPT_WRAPPER();
extern void TIMER_CHIP_INTERRUPT_WRAPPER();

#endif
