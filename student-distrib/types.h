/* types.h - Defines to use the familiar explicitly-sized types in this
 * OS (uint32_t, int8_t, etc.).  This is necessary because we don't want
 * to include <stdint.h> when building this OS
 * vim:ts=4 noexpandtab
 */

#ifndef _TYPES_H
#define _TYPES_H

#define NULL 0
#define NAME_SIZE 32
#define N_FILES 63
#define N_DBLOCK 1023
#define FD_SIZE 8

//file dispatch commands
#define OPEN 0
#define CLOSE 1
#define READ 2
#define WRITE 3

//file types
#define RTC_FILE 0
#define DIR_FILE 1
#define FILE_FILE 2

//common file descriptors
#define STDIN_FD 0
#define STDOUT_FD 1

#define MAX_BUF_SIZE 128
#define HIST_SIZE 10

#define VID_MEM_SIZE  (80 * 25 * 2)

#ifndef ASM

/* Types defined here just like in <stdint.h> */
typedef int int32_t;
typedef unsigned int uint32_t;

typedef short int16_t;
typedef unsigned short uint16_t;

typedef char int8_t;
typedef unsigned char uint8_t;

typedef int bool_t;
enum {FALSE = 0, TRUE = !FALSE};

typedef struct dir_entry {
    uint8_t fname[NAME_SIZE];
    uint32_t ftype;
    uint32_t inode_num;
    uint8_t reserved[24];
} dir_entry_t;

typedef struct boot_block {
    uint32_t num_dir_entry;
    uint32_t num_inodes;
    uint32_t num_datablocks;
    uint8_t reserved[52];
    dir_entry_t dir_entries[N_FILES];
} boot_block_t;

typedef struct inode_block {
    uint32_t length;
    uint32_t data_block_ID[N_DBLOCK];
} inode_t;


//file descriptor structs
typedef struct fd_entry {
	int32_t (*dispatch)(uint32_t, void *, void *, void *); // Pointer to the files operation jump table for read, write, open, and close
	uint32_t inode; // The inode number for the file. Only pertains to data files, not RTC or directory files
	uint32_t file_position; // The position where the file is currently being read at
	bool_t flag; // Flag to set if the current file descriptor entry is in use
} fd_entry_t;

typedef struct __attribute__((packed)) pcb {
  //general purpose regisrers
  uint32_t edi;
  uint32_t esi;
  uint32_t ebp;
  uint32_t esp;
  uint32_t ebx;
  uint32_t edx;
  uint32_t ecx;
  uint32_t eax;

  //iret context
  uint32_t eip;
  uint32_t user_cs;
  uint32_t flags;
  uint32_t iret_esp;
  uint32_t user_ds;

  uint32_t caller_ret;

  uint32_t PID; 		//This is process_number. 0 for shell. increment it everytime

  int32_t next;
  int32_t prev;
  uint32_t tick;
  bool_t is_avaiable;
  bool_t is_active;

  int32_t terminal;
  uint8_t color;

  uint32_t pcb_addr; //probably take off
  uint32_t exec_addr;
  uint32_t ustack_addr;
  uint32_t kstack_addr;

  void * parent_pcb;

  bool_t vidmapped;

  fd_entry_t fd_array[FD_SIZE];
} pcb_t ;


typedef struct terminal{
  volatile bool_t buf_status;
  uint8_t keyboard_buf[MAX_BUF_SIZE];
  uint8_t curr_buf_size;
  
  //History variables
  uint8_t hist[HIST_SIZE][MAX_BUF_SIZE]; //Array storing last 10 instructions typed
  uint8_t hist_buf_size[HIST_SIZE];		//Array storing last 10 buf_sizes
  uint8_t hist_counter;					//Counter tracking write in history. Always increments. Must be INT
  uint8_t hist_cnt_read;					//Counter tracking which history is being displayed. Must be INT
  //Reverse search variables
  int cnt; 					//For reverse search. This is no of characters typed in string to be compared		
  uint8_t found_buf[MAX_BUF_SIZE]; //Search result that matches
  uint8_t found_buf_size;			//Size of matching instruction
  
  bool_t active;
  bool_t awake;
  int screen_x;
  int screen_y;
  pcb_t* top_process;
  uint8_t color;
  uint8_t* vid_mem;

} terminal_t;


#endif /* ASM */

#endif /* _TYPES_H */
