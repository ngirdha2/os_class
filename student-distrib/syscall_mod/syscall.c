#include "syscall.h"
#include "task_switch.h"
#include "../mem_mod/file_descriptor.h"
#include "../mem_mod/fs_driver.h"
#include "../mem_mod/paging.h"
#include "../lib.h"
#include "../interrupt_mod/PIT_driver.h"

// USE MiB and KiB not MB or KB
#define KERNEL_END        0x800000  //8MB
#define PROGRAM_BASE     0x8000000  //128MB - 1st Process Base
#define PROCESS_OFFSET     0x48000
#define PAGE_SIZE         0x400000   //4 MB
#define KS_SIZE             0x2000   //8KB

#define MAX_SIZE          0x3B8000   //4MB - 0x48000
#define EXEC_CHECK_0          0x7f
#define ARG_SIZE               128
#define ENTRY_POINT_OFFSET      24
#define SECURITY_CHECK          22
#define MAX_FD                   7

#define VID_MEM_SIZE  (80 * 25 * 2)

#define VMEM_PAGE_ADDR   0x84B8000

extern void set_kernel_stack(uint32_t esp);
extern int32_t remove_terminal(uint32_t terminal_ID);
extern void switch_terminal_page(pcb_t* terminal);
extern void set_terminal_top_process(pcb_t* process);
extern terminal_t* get_process_terminal();
extern void set_displayed_terminal(uint32_t terminal);

static uint8_t arg_buf[ARG_SIZE];


//static uint8_t vid_mem[VID_MEM_SIZE];
//static int screen_x, screen_y;


/* HALT system call - This system call halts user process
 * input - status which will be returned, value to return from userland
 * output - nothing
 * return value - nothing
 * side effects - kills the user process
 */
uint32_t halt(/*uint8_t status*/){
  pcb_t* process;
  if((process = get_curr_pcb()) == 0) return -1; //Get PCB of current process which is to be halted

  return system_halt(/*(uint32_t) status,*/ process);
}


/* Wrapper to call HALT system call
 * input - status
 * output - nothing
 * return value - nothing
 * side effects - kills the user process
 */
 uint32_t system_halt(/*uint32_t status,*/ pcb_t* process){
  set_terminal_top_process(process->parent_pcb);

  if(process->vidmapped){
    process->vidmapped = FALSE;
    //terminal_t* terminal = get_process_terminal();
    //uint8_t* vmem = terminal->vid_mem;
   // memcpy((uint8_t*)VMEM_PAGE_ADDR, vid_mem, VID_MEM_SIZE);
    //set_x(terminal->screen_x);
    //set_y(terminal->screen_y);

  
  }


  if(process->parent_pcb == NULL){
    remove_process(process->PID); //clean pcb and remove from queue
    return remove_terminal(process->terminal);


  } else ((pcb_t*)process->parent_pcb)->is_active = TRUE;




  pcb_t* parent_process = ((pcb_t*)process->parent_pcb);
  change_process_page(parent_process->PID);

  remove_process(process->PID); //clean pcb and remove from queue

  set_kernel_stack(parent_process->kstack_addr);
  return process->caller_ret;
}


/* EXECUTE system call - This system call starts user process
 * input - filename of user process
 * output - nothing
 * return value - -1 if fail
         return val from main of user process if succesful
         256 if program killed due to exception
 * side effects - increases PID and starts process
 */
int32_t execute(const uint8_t* command){
  //printf("%ds\n", command);
  pcb_t* parent_process = get_curr_pcb();

  return system_execute(command, parent_process, 0);
}

/* EXECUTE system call - This system call starts user process
 * input - filename of user process
 * output - nothing
 * return value - -1 if fail
         return val from main of user process if succesful
         256 if program killed due to exception
 * side effects - increases PID and starts process
 */
 int32_t system_execute(const uint8_t* command, pcb_t* parent_process, uint32_t terminal_ID){
  if(command == NULL) return 0;

  int i;
  dir_entry_t file;
  uint32_t entry_point = 0; //This is where func starts from. bits 24 to 27
  uint32_t file_size;
  pcb_t* process;

  // parse the command
  for(i = 0;;i++) if(command[i] == ' ' || command[i] == '\0' || command[i] == '\n') break;

  uint8_t filename[i+1];
  memset(filename,  '\0',  i+1);
  strncpy((int8_t*)filename, (int8_t*)command, i);
  strcpy((int8_t *)arg_buf, (int8_t*)(command + i + 1));


  // find file
  if(0 != get_dir_entry_by_name(filename, &file)) return -1;
  if((file_size = get_file_size(filename)) > MAX_SIZE) return -1; //File won't fit in (4MB-offset) page


  // check if executable
  uint8_t buf[4] = {0}; //uint8_t buf[file_size];
  if(read_data(file.inode_num, 0, buf, 4) != 4) return -1;
  if((buf[0] != EXEC_CHECK_0) || (buf[1] !='E') || (buf[2] != 'L') || (buf[3] != 'F')) return -1;

  //check if process can be added to the queue else return
  if((process = schedule_process(parent_process, terminal_ID)) == NULL) return 0;


  change_terminal_page(process->terminal); //ADD THIS TO CHANGE WHERE THE VIDEO MEMORY IS BEING WRITTEN
  //set_displayed_terminal(0);

  // map exec page
  change_process_page(process->PID);


  // copy program into memory
  if(read_data(file.inode_num, 0, (uint8_t *)process->exec_addr, file_size) != file_size) return -1;

  // grab the entry point
  if(memcpy((uint8_t*) &entry_point, (uint8_t*)process->exec_addr + ENTRY_POINT_OFFSET, 4) == 0) return -1;

  // perform priviledge switch
  set_kernel_stack(process->kstack_addr);





  uint32_t status = privilege_switch(entry_point, process->ustack_addr, (uint32_t*)process);

  // return status given by halt
  return status;
}

/* OPEN system call - This opens the file and adds the FD entry for it
 * input - filename to be opened
 * output - nothing
 * return value - -1 if failed
         fd if succesful
 * side effects - modifies the File Descriptor array
 */
int32_t open(const uint8_t* filename){
  //printf("\nname %s", filename);

  if(filename == 0) return -1;
  if(!strncmp((int8_t*)filename, "", 1)) return -1;

  int fd;
  dir_entry_t file;
  if(0 == get_dir_entry_by_name(filename, &file)){
    if(0 > (fd = add_fd_entry(file))) return -1;
    get_fd_entry(fd).dispatch(OPEN, 0, 0, 0);
    //printf("%d\n", fd);
    return fd;
  }
  return -1;
}

/* CLOSE system call closes the file, and undos the OPEN system call
 * input - file descriptor
 * output - none
 * return value - 0
 * side effects - sets data structures for file to 0
 */
int32_t close(uint32_t fd){
  if(fd > MAX_FD) return -1;
  if(fd == STDIN_FD || fd == STDOUT_FD) return -1;

  int32_t ret1, ret2;
  fd_entry_t file = get_fd_entry(fd);

  if(file.flag == FALSE) return -1;

  ret1 = file.dispatch(CLOSE, 0, 0, 0);
  ret2 = remove_entry(fd);
  if(ret1 == -1 || ret2 == -1) return -1;
  else return 0;
}



/* READ system call reads Data from file
 *   input - nbytes - is num of bytes to read
 *        - fd - file descriptor
 *        - buf - empty buffer whch will be loaded by function
 *  output - buf
 *  return value - Returns number of bytes read or 0 if none read
 *  side effects - none
 */
int32_t read(uint32_t fd, void* buf, int32_t nbytes){
  if(fd > MAX_FD) return -1;
  if(fd == STDOUT_FD) return -1;
  if(buf == NULL) return -1;  //checking for NULL pointer
  if(nbytes <= 0) return 0;

//  uint32_t flags;
//  asm volatile("pushfl; popl %0" : "=r"(flags));
//  printf("flags 0x%x\n", flags);

  //Initilizing inside Read func so dont do it here // memset(buf,'\0', nbytes);
  fd_entry_t file;
  file = get_fd_entry(fd);
  if(file.flag == FALSE) return -1;
  int32_t bytes = file.dispatch(READ, &fd, buf, &nbytes);
  return bytes;
}

/* WRITE system call - For 391 this does nothing and returns -1
 * input - file descriptor, buf and num of bytes to write
 * output - nothing
 * return value - -1
 * side effects - none
 */
int32_t write(uint32_t fd, const void* buf, int32_t nbytes){
  if(fd > MAX_FD) return -1;
  if(fd == STDIN_FD) return -1;
  if(buf == NULL) return -1;   //checking for NULL pointer
  if(nbytes <= 0) return 0;
  //printf("WRITE\n");


  fd_entry_t file;
  file = get_fd_entry(fd);
  if(file.flag == FALSE) return -1;
  return file.dispatch(WRITE, &fd, (void*) buf, &nbytes);
}



/* GETARGS system call - extracts the arguments
 * input - buf - buffer pointer to write into
       nbytes - # of bytes to write
 * output - none
 * return value -   0 if success
          -1 if fail
 * side effects - none
 */
int32_t get_args(uint8_t* buf, int32_t nbytes){
  if(buf == NULL) return -1;  //checking for NULL pointer
  if( (((uint32_t)buf) >> SECURITY_CHECK) == 1) //Return if buf is pointing to KERNEL
    return -1;
  if(nbytes <= 0) return -1;
  if(arg_buf[0] == '\0') return -1;

  memset(buf,'\0', nbytes); //Initialize buffers
  memcpy(buf, arg_buf, nbytes);
  memset(arg_buf,'\0', ARG_SIZE); //Initialize buffers
  return 0;
}



/* VIDMAP systemcall maps the text-mode video memory into user space at pre-set virtual address
 * input - pointer to video memory
 * output - none
 * return value - 0 if success, -1 if fail
 * side effects - none
 */
int32_t vidmap(uint8_t** screen_start){
  if(screen_start == NULL) return -1;
  if( (((uint32_t)screen_start) >> SECURITY_CHECK) == 1){
  //Return if pointing to KERNEL
    return -1;
  }
  pcb_t* pcb;
  pcb = get_curr_pcb();
  if(!pcb->vidmapped){
    pcb->vidmapped = TRUE;
   // terminal_t* terminal = get_process_terminal();
    //uint8_t* vmem = terminal->vid_mem;

   // memcpy(vid_mem, (uint8_t*)VMEM_PAGE_ADDR, VID_MEM_SIZE);
   // terminal->screen_x = get_x();
   // terminal->screen_y = get_y();

    //clear();
  }
  *screen_start = (uint8_t*)VMEM_PAGE_ADDR;

  return 0;
}
