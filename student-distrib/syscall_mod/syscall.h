#ifndef SYSCALL_H
#define SYSCALL_H

#ifndef ASM

#include "../types.h"

uint32_t halt(/*uint8_t status*/);
//uint32_t system_halt(uint32_t status);
int32_t execute(const uint8_t* command);
int32_t open(const uint8_t* filename);
int32_t close(uint32_t fd);
int32_t read(uint32_t fd, void* buf, int32_t nbytes);
int32_t write(uint32_t fd, const void* buf, int32_t nbytes);
int32_t get_args(uint8_t* buf, int32_t nbytes); //Changed arguments
int32_t system_execute(const uint8_t* command, pcb_t* parent_process, uint32_t terminal_ID);
uint32_t system_halt(/*uint32_t status,*/ pcb_t* process);

#endif
#endif
