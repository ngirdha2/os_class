#ifndef TASK_SWITCH_H
#define TASK_SWITCH_H

#include "../types.h"
#include "../x86_desc.h"

#define PROCESS_BASE	0x8000000

extern uint32_t privilege_switch(uint32_t entry, uint32_t esp, uint32_t* pcb);
extern void halt_switch(uint32_t esp, uint32_t status);
extern void context_switch(uint32_t esp, uint32_t* pcb);

#endif
