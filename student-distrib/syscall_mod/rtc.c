#include "rtc.h"
#include "../interrupt_mod/i8259.h"
#include "../lib.h"


volatile bool_t interrupt_occured;


static int32_t rtc_open ();
static int32_t rtc_close ();
static int32_t rtc_read ();
static int32_t rtc_write (const uint32_t freq);

uint32_t rtc_dispatch(uint32_t cmd, void * arg1, void * arg2, void * arg3){
	switch (cmd) {
		case OPEN:
			return rtc_open();
		case CLOSE:
			return rtc_close();
		case READ:
			return rtc_read();
		case WRITE:
			return rtc_write(*((uint32_t*) arg2));
		default:
			return -1;
	}
	return 0;
}

/* RTC init
* Initializes the RTC and prepares it for interrupts
* Input: none
* Output: none
* Side Effects: RTC clock is turned on
*/
void rtc_init(){
	unsigned char prev_b;
	//get previous value of REG_B
	outb(REG_B, RTC_PORT);
	prev_b = inb(CMOS_PORT);
	//enable bit 6 of b to start periodic interrupts
	outb(REG_B, RTC_PORT);
	outb(prev_b | 0x40, CMOS_PORT);
	interrupt_occured = FALSE;
	rtc_write(2);

	enable_irq(RTC_IDT);
}
/* RTC interrupt
* Called when RTC interrupt occurs. Sets interrupt flag to true
* Inupt: none
* Output: none
* Special effects: handles RTC interrupts
*/
void rtc_interrupt(){
	cli();
	//read in contents of reg C for details on interrupts
	outb(REG_C, RTC_PORT);
	inb(CMOS_PORT);
	send_eoi(RTC_IDT);
	//set interupt flag to 1
	interrupt_occured = TRUE;
	sti();
}
/* RTC Read
* Waits for interrupt to occur, then print to screen and return 0
* Input: none
* Output: 0
* Special Effects: none
*/
 static int32_t rtc_read (){
 	//cli();
 	while(1){
		if(interrupt_occured){
			interrupt_occured = FALSE;
			//printf("Interrupt occured\n");
			break;
		}
 	}
 	//sti();
 	return 0;
 }
 /* RTC Write
 * Takes in desired frequency then sets periodic interrupts to that rate
 * Input: desired frequency
 * Output: 0 on acceptable frequency, -1 on frequency outside of desired range
 * Speical effect: inc/dec speed of interrupts
 */
static int32_t rtc_write(const uint32_t freq){
		uint8_t rs;
		//save values of register a that we don't want to change
			outb(REG_A, RTC_PORT);
			unsigned char prev_a = inb(CMOS_PORT);

			switch(freq){
				//kernel limits interups/sec to 1024, so return fail if it exceeds
			case 8192:
			case 4096:
			case 2048:
				return -1;

			case 1024:
				rs = HZ1024;
				break;
			case 512:
				rs = HZ512;
				break;
			case 256:
				rs = HZ256;
				break;
			case 128:
				rs = HZ128;
				break;
			case 64:
				rs = HZ64;
				break;
			case 32:
				rs = HZ32;
				break;
			case 16:
				rs = HZ16;
				break;
			case 8:
				rs = HZ8;
				break;
			case 4:
				rs = HZ4;
				break;
			case 2:
				rs = HZ2;
				break;
			case 0:
				rs = HZ0;
				break;
			default:
				return -1;
			}
		//set 4 lsb of register a[3:0] to the desired frequency
		outb(REG_A, RTC_PORT);
		outb((0xF0 & prev_a) | rs, CMOS_PORT);
		//printf("Frequency is %x\n", rs);
		return 0;
}

/* RTC Open
* Opens the RTC and sets the frequency to 2 Hz
* Input: none
* Ouput: 0
* Special Effect: none
*/
static int32_t rtc_open (){
	rtc_init();
	return 0;
}
/* RTC close
* Closes the RTC
* Input: none
* Output: 0
* Special Effects: sets RTC frequency to 0
*/
static int32_t rtc_close(){
	//rtc_write(0);
	interrupt_occured = FALSE;

	disable_irq(RTC_IDT);
	return 0;
}
