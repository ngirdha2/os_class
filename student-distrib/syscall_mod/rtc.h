#ifndef _RTC_H_
#define _RTC_H_

#include "../types.h"


#define REG_A 			0x8A
#define REG_B			0x8B
#define REG_C			0x8C
#define RTC_PORT		0x70
#define CMOS_PORT		0x71
#define RTC_IDT 		8
//defining frequencies
#define HZ0				0x00
#define HZ2				0x0F
#define HZ4 			0x0E
#define HZ8				0x0D
#define HZ16			0x0C
#define HZ32			0x0B
#define HZ64			0x0A
#define HZ128			0x09
#define HZ256			0x08
#define HZ512			0x07
#define HZ1024			0x06

void rtc_init(void);
void rtc_interrupt(void);

#endif
